#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   plot_generator.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  small script that produce plots
        based on precomputed results for publications

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import numpy as np
import matplotlib.pyplot as plt

Ndofs=[50,100,200,300,400,500,600,700,800,900,1000,1250,1500,1750]
#num_it_pcg=[29,30,40,44,37,49,54,46,53,52,48,47,49,47]
num_it_pcg=[6,6,6,6,6,6,6,6,6,6,6,6,6,6]

time_cg=[0.2086903999999996, 0.5588361859999997, 2.055856448, 4.5716270340000005, 8.157950308000002, 12.862564437, 18.711046092, 25.602524831000004, 33.76354621600001, 42.754860691999994, 52.940286961, 82.93213048899997, 119.230831465, 162.35036615500002]
#time_cgline=Ndofs*0.1
num_it_cg=[634,1670,2893,6584]
contrasts=[100,]
contrasts=[100,]
Ndofs=np.square(Ndofs)

fig = plt.figure()
#plt.plot(Ndofs, num_it_cg, 'x-', label="CG")
plt.plot(Ndofs, num_it_pcg, 'o-',label="PCG")
plt.title(
    'Number of iteration to reach 1e-6 residual norm\n phase contrast = {}'.format(contrasts[0]))
plt.ylabel('Number of iteration')
plt.xlabel('Number of discretisation points $ N $')
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax = plt.gca()
       # ax.set_xlim([1e-6, 1e6])
ax.set_ylim([1, 1e2])
plt.legend()



fig = plt.figure(5)
plt.loglog(Ndofs, Ndofs/1e4, '-.',label="linear")
plt.loglog(Ndofs, time_cg, '-',label="PCG")
plt.title(
    'Number of iteration to reach 1e-6 residual norm\n phase contrast = {}'.format(contrasts[0]))
plt.ylabel('Time [sec]')
plt.xlabel('Number of discretisation points $ N $')
#plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax = plt.gca()
#ax.set_xlim([1e4, 1e6])
#ax.set_ylim([1e0, 1e2])
plt.legend()

plt.show()

