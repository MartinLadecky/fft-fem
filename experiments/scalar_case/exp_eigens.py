#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_eigens.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: spectra of stiffness matrices with
                    and without Preconditioner

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""



import itertools
import matplotlib.pyplot as plt


from materials.materials import *
from domain import *
from domain.domain_PUC import Domain_PUC
from domain.plot_functions import scy_text

print('\n stiffnes matrix spectrum analysis...')

dim = 2
strains = np.identity(dim)
counteris = 1
rhos = [1]
Ns = [9]
i = 0

for dim, Ni, rho in itertools.product([dim], Ns, rhos):
    Y = [9, 6]
    N = np.array(dim * [Ni, ], dtype=np.int)

    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type='conductivity')

    x0 = Domain.get_x0()

    if i == 0:
        mat_0 = conductivity_material_tensor(Domain, kind='random_isotropic')
        mat_1 = conductivity_material_tensor(Domain, kind='random_isotropic', rho=rho)

        i = i + 1
    E = 19
    nu = 0.35
    mat_0 = np.array([[E / 5, nu], [nu, E]])
    print(mat_0)
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_0 * rho, kind='square_inc')  # 'square_inc'
    Domain.mat.val = mat_fun(Domain.X_qp, kind='discontinuous_IP')  #

    Domain.apply_quadrature_weights(var=Domain.mat)

    Domain.get_ref_mat()

    Domain.get_preconditioner()

    strains = np.identity(dim)

    K = Domain.get_stiffness_matrix()

    if not check_symmetric(K):
        print('K is not symmetric')
    MK = Domain.get_Prec_stiffness_matrix()

    eigvals_K = np.real(np.linalg.eigvals(K))
    eigvals_K = eigvals_K / np.max(eigvals_K)
    eigvals_MK = np.real(np.linalg.eigvals(MK))
    eigvals_MK = eigvals_MK / np.max(eigvals_MK)

    plt.figure()
    eig_round = 15
    uniq_eig_K = np.unique(np.round(eigvals_K, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_K.__len__()), np.sort(eigvals_K), 'o',
                 label='K, N={}, uniq={}'.format(eigvals_K.size, uniq_eig_K))
    print(uniq_eig_K)
    uniq_eig_MK = np.unique(np.round(eigvals_MK, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_MK.__len__()), np.sort(eigvals_MK), 'x',
                 label='MK, N={}, uniq={}'.format(eigvals_MK.size, uniq_eig_MK))
    print(uniq_eig_MK)
    plt.title('Eigenvalues\n '
              'p. contrast {} - rounded to {} decimals'.format(scy_text(rho), eig_round))
    plt.legend()
    ax = plt.gca()
    ax.set_yscale('symlog')
    plt.show()

