#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_convergence_N.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: convergence study of CG and PCG for scalar
        problem with various number of degrees of freedom

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""


import copy

from domain.domain_PUC import Domain_PUC
from solvers.solvers import CG, PCG
from tensors.tensor import Tensor
from materials.materials import conductivity_material_tensor, pixel_mat_fun
from domain.plot_functions import *

print(r'\n experiment shows evolution of convergence with respect to number of DOF ')

num_it_PCG = []
num_it_CG = []
homo_PCG = []
homo_CG = []

rhos = [1e2, ] #phase contrast
mat_geom = 'circle_inc'
problem_type = 'conductivity'
Ns = [9, 11, 12]  # ,36,75,94]#
counter = 0
solutions = []
for elem_type in ['linear']:  # 'bilinear',

    num_it_CG = []
    num_it_PCG = []
    num_it_JCG = []

    homo_CG = []
    homo_PCG = []
    homo_JCG = []
    for dim, Ni, rho in itertools.product([2, ], Ns, rhos):
        print(Ni)
        Y = np.ones(dim, dtype=np.float)
        N = np.array(dim * [Ni, ], dtype=np.int)

        Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type, element_type=elem_type)

        mat_0 = conductivity_material_tensor(dom=Domain, kind='ip_mat_0')

        mat_1 = mat_0 * rho

        x0 = Domain.get_x0()
        pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind=mat_geom, problem_type=problem_type)

        Domain.get_ref_mat(mat_type='mean')
        Domain.get_preconditioner()
        Domain.apply_quadrature_weights(var=Domain.mat)
        Domain.get_Jacoby_preconditioner()
        #
        macro_strain = np.array([1, 0, 0][:dim])

        rhs = Domain.get_rhs(macro_strain=macro_strain)
        f2 = copy.deepcopy(rhs)

        solver = dict(tol=1e-6,
                      maxiter=200,
                      alpha=50,
                      approx_omega=False,
                      divcrit=False,
                      )

        sol_CG, info_CG = CG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0), par=solver)
        num_it_CG.append(info_CG['kit'])
        print(info_CG['norm_res'])

        sol_PCG, info_PCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                                P=Domain.apply_preconditioner, par=solver)
        solutions.append(sol_PCG)
        num_it_PCG.append(info_PCG['kit'])
        print(info_PCG['norm_res'])

        sol_JCG, info_JCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                                P=Domain.apply_Jacoby_prec, par=solver)
        num_it_JCG.append(info_JCG['kit'])
        print('JCG number of iteration = {}'.format(info_JCG['kit']))
        print('JCG norm of resid= {}'.format(info_JCG['norm_res']))

        if counter % 1 == 0:
            ## plot residual norm evolution
            plt.figure(counter)
            plt.semilogy(np.arange(info_CG['kit'] + 1), info_CG['res_log'], label="CG")
            plt.semilogy(np.arange(info_PCG['kit'] + 1), info_PCG['res_log'], label="PCG")
            plt.semilogy(np.arange(info_JCG['kit'] + 1), info_JCG['res_log'], label="JCG")
            plt.title('Residual norm - N={}  , N={}'.format(N, Ni ** dim))
            plt.legend()
            plt.show()
        counter += 1
        Domain.get_grad_periodic(var=sol_CG)
        Domain.get_grad_periodic(var=sol_PCG)
        Domain.set_nodal_coord()

        Domain.compute_e_norm(var=sol_CG)
        print('e norma CG = {}'.format(sol_CG.e_norm))

        Domain.compute_e_norm(var=sol_PCG)
        print('e norma PCG = {}'.format(sol_PCG.e_norm))

        print('Min = {}, Max = {}'.format(np.max(sol_CG.val), np.min(sol_CG.val)))
        # plt.show()

        E = Tensor(name='E', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
        E = Domain.get_macro_strain(var=E, strain=macro_strain)

        sol_CG.grad.val = sol_CG.grad.val + E.grad.val
        homogen_CG = Domain.mat * sol_CG.grad

        sol_PCG.grad.val = sol_PCG.grad.val + E.grad.val
        homogen_PCG = Domain.mat * sol_PCG.grad

        homo_CG.append(np.sum(homogen_CG[0]))
        homo_PCG.append(np.sum(homogen_PCG[0]))

        homo_00 = Domain.mat * sol_PCG.grad
        print('PCG homo A00 {} \n homo A10 {} \n'.format(np.sum(homo_00[0]), np.sum(homo_00[1])))

        # pickle.dump(sol.val, open("domain/experiment_data/sol9x9", "wb"))
    plt.show()
    fig = plt.figure()
    plt.plot(Ns, homo_CG, '--', label="CG")
    plt.plot(Ns, homo_PCG, label="PCG")
    plt.title('A_11 homogeneous \n geom {}, contrast= {} '.format(mat_geom,rho))
    plt.legend()
    plt.show()

    plt.figure()
    plt.plot(Ns, num_it_CG, '--', label="CG")
    plt.plot(Ns, num_it_PCG, label="PCG")
    plt.plot(Ns, num_it_JCG, label="JCG")
    plt.xlabel('Number of DOF in x dir')
    plt.ylabel('Number of iteration to reach 1e-6 residual norm')
    plt.title('Resid. norm evolution \n geom {}, contrast= {}'.format(mat_geom, rho))
    plt.legend()
    plt.show()

    plt.figure(counter)
    plt.imshow(sol_PCG.val)
    plt.show()
    plt.figure(counter * 30)
    plt.show()


