#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_kernel.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: convergence comparison for
                    different preconditioner types

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import itertools
import numpy as np
import copy
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from solvers.solvers import CG, PCG
from tensors.tensor import Tensor
from materials.materials import conductivity_material_tensor, pixel_mat_fun

print(r'\n Experiment shows evolution of convergence with respect preconditioner type ')

num_it_PCG = []
num_it_CG = []
homo_PCG = []
homo_CG = []

rhos = [1e-2, ]
mat_geom = 'circle_inc'
problem_type = 'conductivity'
kernels = ['kernel', 'full', 'diagonal']
Ns = [15, ]
counter = 0

for dim, Ni, rho, kernel in itertools.product([2, ], Ns, rhos, kernels):
    print(Ni)
    Y = np.ones(dim, dtype=np.float)
    N = np.array(dim * [Ni, ], dtype=np.int)

    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type)

    mat_0 = conductivity_material_tensor(dom=Domain, kind='ip_mat_0')

    mat_1 = mat_0 * rho

    x0 = Domain.get_x0()
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind=mat_geom)

    Domain.get_ref_mat()
    Domain.get_preconditioner(exp_type=kernel)
    Domain.apply_quadrature_weights(var=Domain.mat)
    #
    macro_strain = np.array([1, 0, 0][:dim])

    rhs = Domain.get_rhs(macro_strain=macro_strain)
    f2 = copy.deepcopy(rhs)

    solver = dict(tol=1e-6,
                  maxiter=500,
                  alpha=50,
                  approx_omega=False,
                  divcrit=False,
                  )

    sol_CG, info_CG = CG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0), par=solver)
    num_it_CG.append(info_CG['kit'])
    print(info_CG['norm_res'])

    sol_PCG, info_PCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                            P=Domain.apply_preconditioner, par=solver)
    num_it_PCG.append(info_PCG['kit'])
    print(info_PCG['norm_res'])

    if counter % 1 == 0:
        ## plot residual norm evolution
        plt.figure()
        plt.semilogy(np.arange(info_CG['kit'] + 1), info_CG['res_log'], label="CG")
        plt.semilogy(np.arange(info_PCG['kit'] + 1), info_PCG['res_log'], label="PCG")
        plt.title('Residual norm - N={}  , N={}'.format(N, Ni ** dim))
        plt.legend()
        plt.title('geom {}, contrast= {}, kernel = {}'.format(mat_geom, rho, kernel))
        plt.show()
    counter += 1
    Domain.get_grad_periodic(var=sol_CG)
    Domain.get_grad_periodic(var=sol_PCG)
    Domain.set_nodal_coord()

    Domain.compute_e_norm(var=sol_CG)
    print('e norma CG = {}'.format(sol_CG.e_norm))

    Domain.compute_e_norm(var=sol_PCG)
    print('e norma PCG = {}'.format(sol_PCG.e_norm))

    print('Min = {}, Max = {}'.format(np.max(sol_CG.val), np.min(sol_CG.val)))
    # plt.show()

    E = Tensor(name='E', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
    E = Domain.get_macro_strain(var=E, strain=macro_strain)

    sol_CG.grad.val = sol_CG.grad.val + E.grad.val
    homogen_CG = Domain.mat * sol_CG.grad

    sol_PCG.grad.val = sol_PCG.grad.val + E.grad.val
    homogen_PCG = Domain.mat * sol_PCG.grad

    homo_CG.append(np.sum(homogen_CG[0]))
    homo_PCG.append(np.sum(homogen_PCG[0]))

    homo_00 = Domain.mat * sol_PCG.grad
    print('PCG homo A00 {} \n homo A10 {} \n'.format(np.sum(homo_00[0]), np.sum(homo_00[1])))

plt.show()
