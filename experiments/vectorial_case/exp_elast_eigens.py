#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_elast_eigens.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: spectra of stiffness matrices with
                    and without Preconditioner

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import itertools
import scipy.linalg
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from materials.materials import *
from materials.materials import pixel_mat_fun, elastic_material_tensor
from domain.plot_functions import scy_text


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


print('\n stiffness matrix generator...')

dim = 2
strains = np.identity(dim)
counteris = 1
rhos = [1510]  # [1e-4 , 10,1e4]  # [10, 1e2, 1e3]
Ns = [7]  # ,6,9, 12, 15, 18]  # [8,9,10,11] #
i = 0
for dim, Ni, rho in itertools.product([dim], Ns, rhos):
    Y = [9, 6]  # np.ones(dim, dtype=np.float)
    N = np.array(dim * [Ni, ], dtype=np.int)

    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type='elastic')

    x0 = Domain.get_x0()

    # if i == 0:
    #     mat_0 = conductivity_material_tensor(Domain, kind='random_isotropic')
    #     print(mat_0)
    #     mat_1 = conductivity_material_tensor(Domain, kind='random_isotropic', rho=rho)
    #     print(mat_1)
    #     i=i+1

    E_0 = 2.4
    poison_0 = 0.35

    K_0, G_0 = get_bulk_and_shear_modulus(E=E_0, poison=poison_0)
    mat_00 = [E_0 / (1 - poison_0) ** 2, ]

    mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
    mat_0_V = compute_Vight_notation(mat_0)
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_0 * rho, kind='circle_inc',
                  problem_type='elastic')  # random


    Domain.apply_quadrature_weights(var=Domain.mat)
    Domain.get_ref_mat(mat_type='mean')
    Domain.get_preconditioner(prec_type='full')

    # strains = np.identity(dim)
    K = Domain.get_stiffness_matrix()

    # K_muSpectre = np.zeros_like(K)
    # nb_nodal = K.shape[0] // 2
    # for i in range(nb_nodal):
    #     for j in range(nb_nodal):
    #         i_x = i + nb_nodal
    #         i_y = i
    #         j_x = j + nb_nodal
    #         j_y = j
    #         K_muSpectre[2 * i, 2 * j] = K[i_x, j_x]
    #         K_muSpectre[2 * i + 1, 2 * j + 1] = K[i_y, j_y]
    #         K_muSpectre[2 * i, 2 * j + 1] = K[i_x, j_y]
    #         K_muSpectre[2 * i + 1, 2 * j] = K[i_y, j_x]


    MK = Domain.get_Prec_stiffness_matrix()
    MK_diag = np.diag(np.diag(MK))

    K_diag=np.diag(np.diag(K))
    Psym=scipy.linalg.fractional_matrix_power(K_diag,-0.5)
    PKsym=np.matmul(np.matmul(Psym,K),Psym)
    JacK=np.matmul(np.linalg.inv(K_diag),K)
    EXP=np.matmul(np.linalg.inv(K_diag),MK)
    Jackoby_M=Domain.get_Jacoby_preconditioner()

    eigvals_K = np.real(np.linalg.eigvals(K))
    # eigvals_K = eigvals_K / np.max(eigvals_K)
    eigvals_MK = np.real(np.linalg.eigvals(MK))
    # eigvals_MK = eigvals_MK / np.max(eigvals_MK)
    eigvals_JK = np.real(np.linalg.eigvals(PKsym))
    eigvals_EXP = np.real(np.linalg.eigvals(EXP))

    plt.figure(np.random.rand())
    eig_round = 15
    uniq_eig_K = np.unique(np.round(eigvals_K, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_K.__len__()), np.sort(eigvals_K), 'o',
                 label='K, N={}, uniq={}'.format(eigvals_K.size, uniq_eig_K))
    print(uniq_eig_K)
    print(eigvals_K.max()/eigvals_K[eigvals_K>1e-10].min())

    uniq_eig_MK = np.unique(np.round(eigvals_MK, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_MK.__len__()), np.sort(eigvals_MK), 'x',
                 label='MK, N={}, uniq={}'.format(eigvals_MK.size, uniq_eig_MK))
    print(uniq_eig_MK)
    print(eigvals_MK.max() / eigvals_MK[eigvals_MK>1e-10].min())

    uniq_eig_JK = np.unique(np.round(eigvals_JK, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_JK.__len__()), np.sort(eigvals_JK), '|',
                 label='PK, N={}, uniq={}'.format(eigvals_JK.size, uniq_eig_JK))
    print(uniq_eig_JK)
    print(eigvals_JK.max() / eigvals_JK[eigvals_JK>1e-10].min())

    #print(np.sort(eigvals_EXP))
    uniq_eig_EXP = np.unique(np.round(eigvals_EXP, decimals=eig_round)).size
    plt.semilogy(np.arange(eigvals_EXP.__len__()), np.sort(eigvals_EXP),
                 label='EXP, N={}, uniq={}'.format(eigvals_EXP.size, uniq_eig_EXP))
    print(uniq_eig_EXP)
    print(eigvals_EXP.max() / eigvals_EXP[eigvals_EXP > 1e-10].min())

    plt.title('Eigenvalues\n '
              'p. contrast {} - rounded to {} decimals'.format(scy_text(rho), eig_round))
    plt.legend()
    ax = plt.gca()
    ax.set_yscale('symlog')
    plt.show()
