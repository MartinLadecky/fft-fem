# !/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_elast_homogen.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: convergence of homogenized material tensor

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import itertools
import copy
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from solvers.solvers import PCG
from tensors.tensor import Tensor
from materials.materials import *

print(r'\n Experiment shows evolution of convergence with respect to number of DOF ')

mat_geom = 'circle_inc'
problem_type = 'elastic'

dims = 2
Ns = [10, 15, 20]
E_0 = 100
poison_0 = 0.3
rho = [1e1]

num_it_PCG = []

homo_PCG = []
for dim, Ni in itertools.product([dims], Ns):
    Y = np.ones(dim, dtype=np.float)
    N = np.array(dim * [Ni, ], dtype=np.int)

    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type)

    K_0, G_0 = get_bulk_and_shear_modulus(E=E_0, poison=poison_0)

    mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
    mat_0_V = compute_Vight_notation(mat_0)
    print(mat_0_V)

    mat_1 = mat_0 * rho
    mat_1_V = compute_Vight_notation(mat_1)
    print(mat_1_V)

    x0 = Domain.get_x0()
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind=mat_geom)

    # plt.matshow(Domain.mat.val[0, 0, 0, 0, 0, 0])
    # plt.colorbar()
    # plt.show()

    Domain.apply_quadrature_weights(var=Domain.mat)
    Domain.get_ref_mat()
    Domain.get_preconditioner(prec_type='full')
    homo_PCG_N = []
    homo_tensor = np.zeros(4 * (dims,))
    for k in np.arange(dims):
        for l in np.arange(dims):
            macro_strain = np.zeros((dims, dims))
            macro_strain[k, l] = 1
            print(macro_strain)

            rhs = Domain.get_rhs(macro_strain=macro_strain)
            f2 = copy.deepcopy(rhs)

            solver = dict(tol=1e-6,
                          maxiter=500,
                          alpha=50,
                          approx_omega=False,
                          divcrit=False,
                          )

            sol_PCG, info_PCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                                    P=Domain.apply_preconditioner, par=solver)
            print('PCG number of iteration = {}'.format(info_PCG['kit']))
            print('PCG norm of resid = {}'.format(info_PCG['norm_res']))

            Domain.get_grad_periodic(var=sol_PCG)
            # Domain.get_grad_periodic(var=sol_PCG)
            Domain.set_nodal_coord()

            Domain.compute_e_norm(var=sol_PCG)
            print('e norma PCG = {}'.format(sol_PCG.e_norm))

            print('Min = {}, Max = {}'.format(np.max(sol_PCG.val), np.min(sol_PCG.val)))
            # plt.show()

            E = Tensor(name='E', N=Domain.N, shape=Domain.u_shape, Y=Domain.Y, multype='vec')
            E = Domain.get_macro_strain(var=E, strain=macro_strain)

            sol_PCG.grad.val = sol_PCG.grad.val + E.grad.val
            homogen_PCG = Domain.mat * sol_PCG.grad

            for i, j in iter_product((dims, dims)):
                homo_tensor[k, l, i, j] = np.sum(homogen_PCG[i, j])

    mat_homo = compute_Vight_notation(homo_tensor)
    print(mat_homo)
    homo_PCG.append(mat_homo)

for i, j in iter_product(homo_PCG[0].shape):
    if i >= j:
        plt.figure()
        plt.plot(Ns, np.asarray(homo_PCG)[slice(None, None), i, j], '--', label="C[{},{}] ".format(i, j))
        plt.title('Homogenised material matrix \n geom {}, contrast= {}'.format(mat_geom, rho))
        plt.legend()
plt.show()
