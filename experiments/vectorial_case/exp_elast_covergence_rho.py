# !/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_elast_convergence_rho.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: convergence study of CG and PCG for elasticity
        problem with various phase contrast

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import itertools
import numpy as np
import copy
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from solvers.solvers import CG, PCG
from materials.materials import get_bulk_and_shear_modulus, pixel_mat_fun, compute_Vight_notation, \
    elastic_material_tensor
from domain.plot_functions import scy_text

print(r'\n Experiment shows evolution of convergence with respect to phase contrast $\rho$')
print('Elasticity ')

num_it_PCG = []
num_it_CG = []
homo_PCG = []

rhos = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6]
Ni = {2: 9,
      3: 10}
dims = [2, ]
mat_geom = 'circle_inc'
problem_type = 'elastic'
counter = 0
plt.figure()
for dim, rho in itertools.product(dims, rhos):
    print(rho)
    Y = np.ones(dim, dtype=np.float)
    N = np.array(dim * [Ni[dim], ], dtype=np.int)
    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type)

    x0 = Domain.get_x0()

    E_0 = 210
    poison_0 = 0.3

    K_0, G_0 = get_bulk_and_shear_modulus(E=E_0, poison=poison_0)

    mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
    mat_0_V = compute_Vight_notation(mat_0)
    print(np.linalg.eigvals(mat_0_V))

    mat_1 = mat_0 * rho  # elastic_material_tensor(dim=dim,K=K_1, mu=G_1)
    mat_1_V = compute_Vight_notation(mat_1)
    print(np.linalg.eigvals(mat_1_V))

    # pixel_mat_fun(Domain, kind='random', rho=rho)
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind=mat_geom)

    Domain.apply_quadrature_weights(var=Domain.mat)
    Domain.get_ref_mat(mat_type='mean')
    Domain.get_preconditioner(prec_type='full')

    macro_strain = np.array([[1, 0, 0][:dim],
                             [0, 0, 0][:dim],
                             [0, 0, 0][:dim]][:dim])

    rhs = Domain.get_rhs(macro_strain=macro_strain)
    f2 = copy.deepcopy(rhs)

    solver = dict(tol=1e-6,
                  maxiter=1000,
                  alpha=50,
                  approx_omega=False,
                  divcrit=False,
                  )

    sol_PCG, info_PCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                            P=Domain.apply_preconditioner, par=solver)
    num_it_PCG.append(info_PCG['kit'])
    print('PCG number of iteration = {}'.format(info_PCG['kit']))
    print('PCG norm of resid= {}'.format(info_PCG['norm_res']))
    print()

    sol_CG, info_CG = CG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0), par=solver)
    num_it_CG.append(info_CG['kit'])
    print('CG number of iteration = {}'.format(info_CG['kit']))
    print('CG norm of resid = {}'.format(info_CG['norm_res']))
    print()
    if counter % 3 == 0:
        ## plot residual norm evolution
        plt.figure(counter)
        plt.semilogy(np.arange(info_PCG['kit'] + 1), info_PCG['res_log'], label='rho={}'.format(scy_text(rho)))
        plt.semilogy(np.arange(info_CG['kit'] + 1), info_CG['res_log'], label='rho={}'.format(scy_text(rho)))
        plt.title('Residual norm - N={}'.format(N))
        plt.legend()
        plt.show()
    counter += 1
    Domain.get_grad_periodic(var=sol_PCG)
    Domain.set_nodal_coord()

    Domain.compute_e_norm(var=sol_PCG)
    print('e norma PCG = {}'.format(sol_PCG.e_norm))

    print('Min = {}, Max = {}'.format(np.max(sol_PCG.val), np.min(sol_PCG.val)))
    # plt.show()

plt.show()


fig = plt.figure()
plt.loglog(rhos, num_it_CG, '--', label="CG")
plt.loglog(rhos, num_it_PCG, label="PCG")
plt.ylabel('Number of iteration to reach 1e-6 residual norm')
plt.xlabel('Phase contrast')
plt.title('Geometry {}, problem_type {}, N= {}'.format(mat_geom, problem_type, N))
plt.legend()


ax = plt.gca()
ax.set_yscale('linear')
ax.set_xlim([1e-6, 1e6])
plt.show()

fig = plt.figure()
plt.loglog(rhos, num_it_CG, '--', label="CG")
plt.loglog(rhos, num_it_PCG, label="PCG")
plt.ylabel('Number of iteration to reach 1e-6 residual norm')
plt.xlabel('Phase contrast')
plt.title('Geometry {}, problem_type {}, N= {}'.format(mat_geom, problem_type, N))
plt.legend()

ax = plt.gca()
ax.set_yscale('symlog')
ax.set_xlim([1e-6, 1e6])
ax.set_ylim([0, 1e6])
plt.show()
