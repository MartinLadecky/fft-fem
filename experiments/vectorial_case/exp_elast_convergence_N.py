#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   exp_elast_convergence_N.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   30 Aug 2020

@brief  Experiment: convergence study of CG and PCG for elasticity
        problem with various number of degrees of freedom

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import itertools
import copy
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from solvers.solvers import PCG
from materials.materials import *

print(r'\n Experiment shows evolution of convergence with respect to number of DOF ')

mat_geom = 'circle_inc'
problem_type = 'elastic'

num_it_CG = []
num_it_PCG = []
num_it_JCG = []

homo_CG = []
homo_PCG = []
homo_JCG = []

rho = 1e3
E_0 = 210
poison_0 = 0.3

Ns = [3,4,5] #10, 15, 20]  # ,45,55,65,75,85
counter = 0
for dim, Ni in itertools.product([2], Ns):
    print(Ni)
    Y = np.ones(dim, dtype=np.float)
    N = np.array(dim * [Ni, ], dtype=np.int)

    Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type)

    K_0, G_0 = get_bulk_and_shear_modulus(E=E_0, poison=poison_0)

    mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
    mat_0_V = compute_Vight_notation(mat_0)

    mat_1 = mat_0 * rho
    mat_1_V = compute_Vight_notation(mat_1)

    x0 = Domain.get_x0()
    pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind=mat_geom, problem_type=problem_type)


    Domain.apply_quadrature_weights(var=Domain.mat)
    Domain.get_ref_mat()
    Domain.get_preconditioner(prec_type='full')
    Domain.get_Jacoby_preconditioner()
    #
    macro_strain = np.array([[1, 0, 0][:dim],
                             [0, 0, 0][:dim],
                             [0, 0, 0][:dim]][:dim])

    rhs = Domain.get_rhs(macro_strain=macro_strain)
    f2 = copy.deepcopy(rhs)

    solver = dict(tol=1e-6,
                  maxiter=500,
                  alpha=50,
                  approx_omega=False,
                  divcrit=False,
                  )

    sol_CG, info_CG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                          P=Domain.apply_Jacoby_prec, par=solver)
    num_it_CG.append(info_CG['kit'])
    print('CG number of iteration = {}'.format(info_CG['kit']))
    print('CG norm of resid = {}'.format(info_CG['norm_res']))
    print()

    sol_PCG, info_PCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                            P=Domain.apply_preconditioner, par=solver)
    num_it_PCG.append(info_PCG['kit'])
    print('PCG number of iteration = {}'.format(info_PCG['kit']))
    print('PCG norm of resid= {}'.format(info_PCG['norm_res']))
    print()

    sol_JCG, info_JCG = PCG(Afun=Domain.lhs_periodic, B=f2, x0=copy.deepcopy(x0),
                            P=Domain.apply_combined_prec, par=solver)
    num_it_JCG.append(info_JCG['kit'])
    print('JCG number of iteration = {}'.format(info_JCG['kit']))
    print('JCG norm of resid= {}'.format(info_JCG['norm_res']))
    print()

    if counter % 3 == 0:
        ## plot residual norm evolution

        plt.figure()
        plt.semilogy(np.arange(info_CG['kit'] + 1), info_CG['res_log'], 'r', label="CG N= {}".format(N))
        plt.semilogy(np.arange(info_PCG['kit'] + 1), info_PCG['res_log'], 'b--', label="PCG N= {}".format(N))
        plt.semilogy(np.arange(info_JCG['kit'] + 1), info_JCG['res_log'], 'g--', label="JCG N= {}".format(N))
        plt.title('Residual norm - N={}  , N={}'.format(N, Ni ** dim))
        plt.legend()
        plt.show()
    counter += 1

    Domain.get_grad_periodic(var=sol_CG)
    Domain.get_grad_periodic(var=sol_PCG)

    Domain.compute_e_norm(var=sol_CG)
    print('e norma CG = {}'.format(sol_CG.e_norm))

    Domain.compute_e_norm(var=sol_PCG)
    print('e norma PCG = {}'.format(sol_PCG.e_norm))

    print('Min = {}, Max = {}'.format(np.max(sol_CG.val), np.min(sol_CG.val)))

fig = plt.figure()
plt.plot(Ns, num_it_CG, '--', label="CG")
plt.plot(Ns, num_it_PCG, label="PCG")
plt.plot(Ns, num_it_JCG, label="JCG")
plt.xlabel('Number of DOF in x dir')
plt.ylabel('Number of iteration to reach 1e-6 residual norm')
plt.title('Resid. norm evolution \n geom {}, contrast= {}'.format(mat_geom, rho))
plt.legend()
plt.show()
