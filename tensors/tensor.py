#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   tensor.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  Tensor object, a multidimensional array object
        that knows how to react with
        other multidimensional objects.

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""
import numpy as np


class Tensor(object):
    def __init__(self, name='', n_epp=(), n_qp=(), val=None, order=None, shape=None, N=None, Y=None,
                 multype='scal', type='scalar'):

        self.name = name

        if isinstance(val, np.ndarray):  # define: val + order
            self.val = val
            self.order = int(order)
            self.shape = self.val.shape[:order]
            self.N = tuple(np.array(N, dtype=np.int))


        elif shape is not None and N is not None:  # define: shape + N
            self.n_epp = tuple(n_epp)
            self.n_qp = tuple(n_qp)
            self.N = tuple(np.array(N, dtype=np.int))
            self.shape = tuple(np.array(shape, dtype=np.int))
            self.order = len(self.shape)
            self.dim = self.N.__len__()
            self.val = np.zeros(
                self.shape + self.n_epp + self.n_qp + tuple(np.asarray(self.N, dtype=int)),
                dtype=np.float)

        else:
            raise ValueError('Initialization of Tensor.')

        if Y is None:
            self.Y = np.ones(self.dim, dtype=np.float)
        else:
            self.Y = np.array(Y, dtype=np.float)

        self.multype = multype

    def __getitem__(self, ii):
        return self.val[ii]

    def __call__(self, *args, **kwargs):
        return self.__mul__(*args, **kwargs)

    def __mul__(self, Y, *args, **kwargs):
        multype = self.multype
        X = self.val
        Y = Y.val
        if multype in ['scal', 'scalar']:
            return np.einsum('i...,i...->...', X, Y)
        elif multype in [21, '21']:
            return np.einsum('ij...,j...->i...', X, Y)
        elif multype in [42, '42']:
            return np.einsum('ijkl...,kl...->ij...', X, Y)
        elif multype in [00, 'elementwise', 'hadamard']:
            return np.einsum('...,...->...', X, Y)
        elif multype in ['grad']:
            return np.einsum('i...,...->i...', X, Y)
        elif multype in ['div']:
            return np.einsum('i...,i...->...', X, Y)
        else:
            raise ValueError()

    def set_nodal_coord(self):
        self.X = np.meshgrid(*[np.linspace(0, 1, self.N[d]) for d in range(0, self.dim)], indexing='ij')

    def make_periodic(self):
        self.val = np.append(self.val, self.val[0:1, :], 0)
        self.val = np.append(self.val, self.val[:, 0:1], 1)
