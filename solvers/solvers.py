#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file  solvers.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  Conjungate Gradients and Preconditioned Conjungate Gradients
        Iterative solvers for systems of linear equation

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""


import numpy as np
import copy

from tensors.tensor import Tensor



def CG(Afun, B, x0, par=None, callback=None):
    """
    Conjugate gradients solver.

    Parameters
    ----------
    Afun : Matrix, LinOper, or numpy.array of shape (n, n)
        it stores the matrix data of linear system and provides a matrix by
        vector multiplication
    B : VecTri or numpy.array of shape (n,)
        it stores a right-hand side of linear system
    x0 : VecTri or numpy.array of shape (n,)
        initial approximation of solution of linear system
    """
    if par is None:
        par = dict()
    if 'tol' not in list(par.keys()):
        par['tol'] = 1e-6
    if 'maxiter' not in list(par.keys()):
        par['maxiter'] = int(500)

    # scal=get_scal(B)
    res = dict()
    ##
    r_0 = Tensor(name='r_0', N=x0.N, shape=x0.shape, Y=x0.Y, multype='scal')
    ##
    xCG = copy.deepcopy(x0)
    xCG.name = 'Solution'
    ##
    Ax = Afun(var=x0)

    r_0.val = B.val - Ax.val

    p_0 = copy.deepcopy(r_0)
    r_0r_0 = scal(r_0, r_0)

    res['kit'] = 0
    res['norm_res'] = np.double(r_0r_0) ** 0.5  # /np.norm(E_N)
    norm_res_log = []
    norm_res_log.append(res['norm_res'])
    # counter=0
    # xCG_sol=copy.deepcopy(xCG)

    while (res['norm_res'] > par['tol']) and (res['kit'] < par['maxiter']):
        res['kit'] += 1  # number of iterations
        Ap_0 = Afun(var=p_0)
        alpha = float(r_0r_0 / scal(p_0, Ap_0))
        xCG.val = xCG.val + alpha * p_0.val

        if xCG.val.mean() > 1e-10:
            print('iteration left zero-mean space {} \n {}'.format(xCG.name, xCG.val.mean()))

        r_0.val = r_0.val - alpha * Ap_0.val

        # print('Res norm {}'.format( np.linalg.norm(r_0.val)))
        r_1r_1 = scal(r_0, r_0)

        if (np.sqrt(r_1r_1)) < par['tol']:
            norm_res_log.append(np.sqrt(r_1r_1))
            break


        beta = r_1r_1 / r_0r_0
        p_0.val = r_0.val + beta * p_0.val

        r_0r_0 = r_1r_1
        res['norm_res'] = np.double(r_0r_0) ** 0.5
        # print(res['norm_res'])
        norm_res_log.append(res['norm_res'])

        if callback is not None:
            callback(xCG)

    res['res_log'] = norm_res_log
    return xCG, res


def PCG(Afun, B, x0, P, par=None, callback=None):
    """
    Conjugate gradients solver.

    Parameters
    ----------
    Afun : Matrix, LinOper, or numpy.array of shape (n, n)
        it stores the matrix data of linear system and provides a matrix by
        vector multiplication
    B : VecTri or numpy.array of shape (n,)
        it stores a right-hand side of linear system
    x0 : VecTri or numpy.array of shape (n,)
        initial approximation of solution of linear system
    """
    if par is None:
        par = dict()
    if 'tol' not in list(par.keys()):
        par['tol'] = 1e-6
    if 'maxiter' not in list(par.keys()):
        par['maxiter'] = int(500)

    # scal=get_scal(B)
    res = dict()
    ##
    r_0 = Tensor(name='r_0', N=x0.N, shape=x0.shape, Y=x0.Y, multype='scal')
    ##
    xCG = copy.deepcopy(x0)
    xCG.name = 'Solution'
    ##
    Ax = Afun(var=x0)

    r_0.val = B.val - Ax.val
    #print('r_0 =  {}'.format(r_0.val))
    z_0 = P(var=r_0)
    #print('r_0 =  {}'.format(r_0.val))
    p_0 = copy.deepcopy(z_0)

    r_0r_0 = scal(r_0, r_0)
    #print('r_0r_0 =  {}'.format(r_0r_0))
    r_0z_0 = scal(r_0, z_0)
    #print('r_0z_0 =  {}'.format(r_0z_0))

    res['kit'] = 0
    res['norm_res'] = np.double(r_0r_0) ** 0.5  # /np.norm(E_N)
    norm_res_log = []
    norm_res_log.append(res['norm_res'])

    while (res['norm_res'] > par['tol']) and (res['kit'] < par['maxiter']):
        res['kit'] += 1  # number of iterations
        Ap_0 = Afun(var=p_0)
        alpha = float(r_0z_0 / scal(p_0, Ap_0))
        xCG.val = xCG.val + alpha * p_0.val

        if xCG.val.mean() > 1e-10:
            print('iteration left zero-mean space {} \n {}'.format(xCG.name, xCG.val.mean()))

        r_0.val = r_0.val - alpha * Ap_0.val

        r_1r_1 = scal(r_0, r_0)

        if (np.sqrt(r_1r_1)) < par['tol']:
            norm_res_log.append(np.sqrt(r_1r_1))
            break

        z_0 = P(var=r_0)
        r_1z_1 = scal(r_0, z_0)
        beta = r_1z_1 / r_0z_0
        p_0.val = z_0.val + beta * p_0.val

        r_0z_0 = r_1z_1
        r_0r_0 = r_1r_1
        res['norm_res'] = np.double(r_0r_0) ** 0.5
        norm_res_log.append(res['norm_res'])

        if callback is not None:
            callback(xCG)

    res['res_log'] = norm_res_log
    return xCG, res


def scal(X, Y):
    if X.shape == ():
        if len(X.val.shape) == 1:
            scalar = np.einsum('i...,i...->...', X.val, Y.val)
        elif len(X.val.shape) == 2:
            scalar = np.einsum('ij...,ij...->...', X.val, Y.val)
        elif len(X.val.shape) == 3:
            scalar = np.einsum('ijk...,ijk...->...', X.val, Y.val)
    elif X.shape == (X.dim,):
        # for disp in range(*X.u.shape):
        if len(X.val.shape) == 2:
            scalar = np.einsum('ij...,ij...->...', X.val, Y.val)
        elif len(X.val.shape) == 3:
            scalar = np.einsum('ijk...,ijk...->...', X.val, Y.val)
        elif len(X.val.shape) == 4:

            scalar = np.einsum('ijkl...,ijkl...->...', X.val, Y.val)
    return scalar

