#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   materials.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  functions to generate material tensors as well as global material matrix

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import numpy as np
from random import choice

from domain import iter_product


def pixel_mat_fun(dom, mat_0=None, mat_1=None, kind='constant', problem_type='conductivity'):
    """
    Functions assemble global material tensor:
        -scalar: A[dim,dim,n_el,n_qp,*N] - second order tensor in every quad. point
        -vector: C[dim,dim,dim,dim,n_el,n_qp,*N] - fourth order tensor in every quad. point

    Arguments:
    dom             -- Domain object from domain_PUC.py
    mat_0           -- first material tensor
    mat_1           -- second material tensor
    kind            -- define geometry : 'constant', 'square_inc', 'circle_inc', 'random', 'square_inc_qp', 'circle_inc_qp'
    problem_type    -- define what kind of problem is solved: 'conductivity' (scalar case) or 'elastic' (vector case)

    Returns:
        dom.mat instance
     """

    if not kind in ['constant', 'square_inc', 'circle_inc', 'random', 'square_inc_qp', 'circle_inc_qp', 'random_geom']:
        raise ValueError('Unrecognised materiel kind')
    for pixel in iter_product(dom.mat.N):
        for el_coor in iter_product(dom.mat.n_epp + dom.mat.n_qp):
            index = len(dom.mat.shape) * (slice(None, None),) + el_coor + pixel
            if kind in 'constant':
                dom.mat.val[index] = mat_0

            elif kind in 'square_inc':
                if mat_0 is None or mat_1 is None:
                    raise ('Square_inc needs two materials; ')
                center = dom.Y / 2
                inc_size = np.array(dom.dim * [0.22, ][:dom.dim])
                distances = np.array(
                    [abs(center[d] - dom.X[d][tuple(pixel)] + dom.h[0] / 2) for d in np.arange(dom.dim)])
                dom.mat.val[index] = mat_0 if (distances < inc_size).all() else mat_1

            elif kind in 'square_inc_qp':
                if mat_0 is None or mat_1 is None:
                    raise ('Square_inc_qp needs two materials; ')
                center = dom.Y / 2
                inc_size = np.array(dom.dim * [0.22, ][:dom.dim])
                distances = np.array([abs(center[d] - dom.X_qp[d][tuple(el_coor + pixel)]) for d in np.arange(dom.dim)])
                dom.mat.val[index] = mat_0 if (distances < inc_size).all() else mat_1

            elif kind in 'circle_inc':
                center = dom.Y / 2
                inc_diameter = 0.2
                dists = [(center[d] - dom.X[d][tuple(pixel)] + dom.h[0] / 2) ** 2 for d in np.arange(dom.dim)]
                dist = np.sqrt(np.sum(dists))
                dom.mat.val[index] = mat_0 if dist < inc_diameter else mat_1

            elif kind in 'circle_inc_qp':
                center = dom.Y / 2
                inc_diameter = 0.2
                dists = [(center[d] - dom.X_qp[d][tuple(el_coor + pixel)]) ** 2 for d in np.arange(dom.dim)]
                dist = np.sqrt(np.sum(dists))
                dom.mat.val[index] = mat_0 if dist < inc_diameter else mat_1

            elif kind in 'random':
                if problem_type == 'conductivity':
                    dom.mat.val[index] = conductivity_material_tensor(dom, kind='random')
                elif problem_type == 'elastic':
                    dom.mat.val[index] = elastic_material_tensor(dom.dim, kind='linear') * np.random.rand()
            elif kind in 'random_geom':
                if problem_type == 'conductivity':
                    first = choice(['True', 'False'])
                    if first:
                        dom.mat.val[index] = mat_0
                    else:
                        dom.mat.val[index] = mat_1
                elif problem_type == 'elastic':
                    first = choice(['True', 'False'])
                    # print(first)
                    if first:
                        dom.mat.val[index] = mat_0
                    else:
                        dom.mat.val[index] = mat_1


def conductivity_material_tensor(dom, rho=1, kind='identity'):
    mat = np.array(dom.dim * [dom.dim, ])
    if kind in 'identity':
        mat = np.eye(dom.dim)
    elif kind in 'random':
        mat = rho * 10 * np.eye(dom.dim) * np.random.random(2 * [dom.dim, ]) + \
              (np.ones(dom.dim) - np.eye(dom.dim)) * np.random.random(2 * [dom.dim, ])
        mat = (mat + mat.T) / 2
        if not np.all(np.linalg.eigvals(mat) > 0):
            mat += np.diag(mat)
    elif kind in 'random_isotropic':
        mat = rho * 10 * np.eye(dom.dim) * np.random.random(2 * [dom.dim, ])
    elif kind in 'ip_mat_0':
        mat = np.array([[5. * rho, -4 * rho], [-4 * rho, 5 * rho]])
    elif kind in 'ip_mat_1':
        mat = np.array([[2, -1], [-1, 2]])

    return mat


def elastic_material_tensor(dim, K=1, mu=0.5, kind='linear'):
    shape = np.array(4 * [dim, ])
    mat = np.zeros(shape)
    kron = lambda a, b: 1 if a == b else 0

    if kind in 'linear':
        for alpha, beta, gamma, delta in iter_product(shape):
            mat[alpha, beta, gamma, delta] = (K * (kron(alpha, beta) * kron(gamma, delta))
                                              + mu * (kron(alpha, gamma) * kron(beta, delta) +
                                                      kron(alpha, delta) * kron(beta, gamma) -
                                                      2 / 3 * kron(alpha, beta) * kron(gamma, delta)))

    return mat


def reference_pixel_mat_fun(dom, kind='constant', rho=1):
    ## Matrial functions used for testing
    #
    # DO NOT CHANGE
    #
    ##

    val = np.zeros(dom.mat.shape)
    for pixel in iter_product(dom.mat.N):
        for el_coor in iter_product(dom.mat.n_epp + dom.mat.n_qp):
            # print(pixel)
            if kind in 'constant':
                dom.mat.val[0][0][el_coor + pixel] = 1
                dom.mat.val[1][1][el_coor + pixel] = 1

            elif kind in 'discontinuous':
                if dom.dim == 2:
                    if (pixel[0] >= dom.mat.N[0] / 3 and
                            pixel[0] < dom.mat.N[0] * 2 / 3 and
                            pixel[1] >= dom.mat.N[1] / 3 and
                            pixel[1] < dom.mat.N[1] * 2 / 3):
                        # print(dom.X[0][pixel], dom.X[1][pixel])
                        dom.mat.val[0][0][el_coor + pixel] = 10 * rho
                        dom.mat.val[1][1][el_coor + pixel] = 5 * rho
                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 3 * rho

                    else:
                        dom.mat.val[0][0][el_coor + pixel] = 1
                        dom.mat.val[1][1][el_coor + pixel] = 0.5
                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0.3

                if dom.dim == 3:
                    if (pixel[0] >= dom.mat.N[0] / 3 and
                            pixel[0] < dom.mat.N[0] * 2 / 3 and
                            pixel[1] >= dom.mat.N[1] / 3 and
                            pixel[1] < dom.mat.N[1] * 2 / 3 and
                            pixel[2] >= dom.mat.N[2] / 3 and
                            pixel[2] < dom.mat.N[2] * 2 / 3):
                        # print(dom.X[0][pixel], dom.X[1][pixel])
                        dom.mat.val[0][0][el_coor + pixel] = 10 * rho
                        dom.mat.val[1][1][el_coor + pixel] = 7 * rho
                        dom.mat.val[2][2][el_coor + pixel] = 5 * rho

                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 3 * rho
                        dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 5 * rho
                        dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 4 * rho

                    else:
                        dom.mat.val[0][0][el_coor + pixel] = 1
                        dom.mat.val[1][1][el_coor + pixel] = 0.7
                        dom.mat.val[2][2][el_coor + pixel] = 0.5

                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0.3
                        dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 0.5
                        dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 0.4

            elif kind in 'discontinuous_test':
                if dom.dim == 2:
                    if (pixel[0] >= dom.mat.N[0] / 3 and
                            pixel[0] < dom.mat.N[0] * 2 / 3 and
                            pixel[1] >= dom.mat.N[1] / 3 and
                            pixel[1] < dom.mat.N[1] * 2 / 3):
                        # print(dom.X[0][pixel], dom.X[1][pixel])
                        dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                        dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0 * rho

                    else:
                        dom.mat.val[0][0][el_coor + pixel] = 1
                        dom.mat.val[1][1][el_coor + pixel] = 1
                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0

                if dom.dim == 3:
                    if (pixel[0] >= dom.mat.N[0] / 3 and
                            pixel[0] < dom.mat.N[0] * 2 / 3 and
                            pixel[1] >= dom.mat.N[1] / 3 and
                            pixel[1] < dom.mat.N[1] * 2 / 3 and
                            pixel[2] >= dom.mat.N[2] / 3 and
                            pixel[2] < dom.mat.N[2] * 2 / 3):
                        # print(dom.X[0][pixel], dom.X[1][pixel])
                        dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                        dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                        dom.mat.val[2][2][el_coor + pixel] = 1 * rho

                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0 * rho
                        dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 0 * rho
                        dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 0 * rho

                    else:
                        dom.mat.val[0][0][el_coor + pixel] = 1
                        dom.mat.val[1][1][el_coor + pixel] = 1
                        dom.mat.val[2][2][el_coor + pixel] = 1

                        dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0
                        dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 0
                        dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 0


            elif kind in 'identity':
                if dom.dim == 2:
                    dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                    dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                    dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0 * rho

                if dom.dim == 3:
                    dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                    dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                    dom.mat.val[2][2][el_coor + pixel] = 1 * rho

                    dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0 * rho
                    dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 0 * rho
                    dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 0 * rho

            elif kind in 'unity':
                if dom.dim == 2:
                    dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                    dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                    dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0.1 * rho

                if dom.dim == 3:
                    dom.mat.val[0][0][el_coor + pixel] = 1 * rho
                    dom.mat.val[1][1][el_coor + pixel] = 1 * rho
                    dom.mat.val[2][2][el_coor + pixel] = 1 * rho

                    dom.mat.val[0][1][el_coor + pixel] = dom.mat.val[1][0][el_coor + pixel] = 0 * 0.1 * rho
                    dom.mat.val[0][2][el_coor + pixel] = dom.mat.val[2][0][el_coor + pixel] = 0 * 0.1 * rho
                    dom.mat.val[1][2][el_coor + pixel] = dom.mat.val[2][1][el_coor + pixel] = 0 * 0.1 * rho


def mat_fun(x, kind='constant', dim=2, rho=1):
    val = np.zeros(tuple(2 * (x.__len__(),)) + x[0].shape)
    for x_i in np.ndindex(x[0].shape):
        if kind in 'constant':
            val[0][0][x_i] = 1
            val[1][1][x_i] = 1

        elif kind in 'linear':
            val[0][0][x_i] = x[0][x_i]  # + 3 * x[1][x_i]
            val[1][1][x_i] = 2 * x[1][x_i]  # - x[1][x_i]

        elif kind in 'peak':
            if dim == 2:
                val[0][0][x_i] = 10 + 100 * (-abs(2 * x[0][x_i] - 1) + 1) * (
                        -abs(2 * x[1][x_i] - 1) + 1)  # + 3 * x[1][x_i]
                val[1][1][x_i] = 10 + 1 * (-abs(2 * x[0][x_i] - 1) + 1) * (-abs(2 * x[1][x_i] - 1) + 1)  # - x[1][x_i]
            elif dim == 3:
                val[0][0][x_i] = 10 + rho * (-abs(2 * x[0][x_i] - 1) + 1) * (-abs(2 * x[1][x_i] - 1) + 1) * (
                        -abs(2 * x[2][x_i] - 1) + 1)
                val[1][1][x_i] = 5 + rho * (-abs(2 * x[0][x_i] - 1) + 1) * (-abs(2 * x[1][x_i] - 1) + 1) * (
                        -abs(2 * x[2][x_i] - 1) + 1)
                val[2][2][x_i] = 13 + rho * (-abs(2 * x[0][x_i] - 1) + 1) * (-abs(2 * x[1][x_i] - 1) + 1) * (
                        -abs(2 * x[2][x_i] - 1) + 1)


        elif kind in 'bilinear':
            val[0][0][x_i] = x[0][x_i] * x[1][x_i]
            val[1][1][x_i] = x[0][x_i] * x[1][x_i]

        elif kind in 'quadratic':
            val[0][0][x_i] = 4 + 10 * ((2 * x[0][x_i] - 1) ** 2) * ((2 * x[1][x_i] - 1) ** 2)
            val[1][1][x_i] = 2 + ((2 * x[0][x_i] - 1) ** 2) * ((2 * x[1][x_i] - 1) ** 2)

        elif kind in 'periodic':
            val[0][0][x_i] = 10 + np.cos(2 * np.pi * x[0][x_i]) * (2 * x[0][x_i] - 1) ** 2
            val[1][1][x_i] = 5 + np.cos(2 * np.pi * x[1][x_i]) * (2 * x[1][x_i] - 1) ** 2

        elif kind in 'periodic_IP':
            val[0][0][x_i] = 1 + np.sin(2 * np.pi * x[0][x_i]) / 2
            val[1][1][x_i] = 2 + np.sin((2 * np.pi * x[0][x_i]) / 2) / 2
            val[0][1][x_i] = 0.3
            val[1][0][x_i] = 0.3

        elif kind in 'discontinuous':
            if ((abs(x[0][x_i]) > 1 / 3 and abs(x[1][x_i]) > 1 / 3) and \
                    (abs(x[0][x_i]) < 2 / 3 and abs(x[1][x_i]) < 2 / 3)):
                val[0][0][x_i] = 100
                val[1][1][x_i] = 100
                val[0][1][x_i] = 30
                val[1][0][x_i] = 30
            else:
                val[0][0][x_i] = 10
                val[1][1][x_i] = 10
                val[0][1][x_i] = 4
                val[1][0][x_i] = 4

        elif kind in 'ref_discontinuous':
            if (abs(x[0][x_i]) > 0.3 and abs(x[1][x_i]) > 0.3) and (abs(x[0][x_i]) < 0.7 and abs(x[1][x_i]) < 0.7):
                val[0][0][x_i] = 10
                val[1][1][x_i] = 10
                val[0][1][x_i] = 0
                val[1][0][x_i] = 0
            else:
                val[0][0][x_i] = 1
                val[1][1][x_i] = 1
                val[0][1][x_i] = 0
                val[1][0][x_i] = 0

        elif kind in 'discontinuous_IP':
            if (abs(x[0][x_i]) > 1 / 6 and abs(x[1][x_i]) > 1 / 6) and \
                    (abs(x[0][x_i]) < 5 / 6 and abs(x[1][x_i]) < 5 / 6):
                val[0][0][x_i] = 5
                val[1][1][x_i] = 5
                val[0][1][x_i] = -4
                val[1][0][x_i] = -4
            else:
                val[0][0][x_i] = 2
                val[1][1][x_i] = 2
                val[0][1][x_i] = -1
                val[1][0][x_i] = -1
    return val


def compute_Vight_notation(C):
    # function return Voigt notation of elastic tensor in quad. point
    if len(C) == 2:
        C_voigt = np.zeros([3, 3])
        i_ind = [(0, 0), (1, 1), (0, 1)]
        for i in np.arange(len(C_voigt[0])):
            for j in np.arange(len(C_voigt[1])):
                # print()
                # print([i_ind[i]+quad_point+i_ind[j]+quad_point])
                C_voigt[i, j] = C[i_ind[i] + i_ind[j]]

    elif len(C) == 3:
        C_voigt = np.zeros([6, 6])
        i_ind = [(0, 0), (1, 1), (2, 2), (1, 2), (0, 2), (0, 1)]
        for i in np.arange(len(C_voigt[0])):
            for j in np.arange(len(C_voigt[1])):
                C_voigt[i, j] = C[i_ind[i] + i_ind[j]]
                # print()
    return C_voigt


def get_bulk_and_shear_modulus(E, poison):
    K = E / (3 * (1 - 2 * poison))
    G = E / (2 * (1 + poison))
    return K, G
