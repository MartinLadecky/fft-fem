#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   unittest_domain_PUC.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  unittests for whole project

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import unittest
import itertools
import copy
import matplotlib.pyplot as plt

from domain.domain_PUC import Domain_PUC
from tensors.tensor import Tensor
from materials.materials import *
from domain import iter_product
from domain import check_symmetric


class MyTestCase(unittest.TestCase):

    def test_grad_periodic(self):
        print('\nChecking new gradient operator ...')
        for dim, Ni in itertools.product([3], [5]):
            Y = [1, 2, 3][:dim]
            N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y)

            Domain.v = Tensor(name='v', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
            Domain.v.val = (3 * Domain.X[0]) + (4 * Domain.X[1]) - (6 * Domain.X[2])
            Domain.get_grad_periodic(var=Domain.v)

            Domain.u = Tensor(name='u', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
            Domain.u.val = 0 * Domain.X[0]
            Domain.get_grad_periodic(var=Domain.u)
            Domain.u.grad.val[0] = 3  #
            Domain.u.grad.val[1] = 4  #
            Domain.u.grad.val[2] = -6  #

            np.testing.assert_array_almost_equal(Domain.u.grad.val[0, :, :, :-1, :, :],
                                                 Domain.v.grad.val[0, :, :, :-1, :, :], decimal=1)
            np.testing.assert_array_almost_equal(Domain.u.grad.val[1, :, :, :, :-1, :],
                                                 Domain.v.grad.val[1, :, :, :, :-1, :], decimal=1)
            np.testing.assert_array_almost_equal(Domain.u.grad.val[2, :, :, :, :, :-1],
                                                 Domain.v.grad.val[2, :, :, :, :, :-1], decimal=1)

    def test_grad_periodic_2D(self):
        print('\nChecking new gradient operator ...')
        for dim, Ni in itertools.product([2], [5]):
            Y = [5, 4, 3][:dim]
            N = np.array([Ni, Ni+1 , Ni + 2], dtype=np.int)[:dim]

            Domain = Domain_PUC(name='Domain', N=N, Y=Y,element_type='bilinear')

            Domain.v = Tensor(name='v', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
            Domain.v.val = (3 * Domain.X[0]) + (4 * Domain.X[1])
            Domain.get_grad_periodic(var=Domain.v)

            Domain.u = Tensor(name='u', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
            Domain.u.val = 0 * Domain.X[0]
            Domain.get_grad_periodic(var=Domain.u)
            Domain.u.grad.val[0] = 3  #
            Domain.u.grad.val[1] = 4  #

            print(Domain.v.grad.val[0, 0, 0])
            print(Domain.v.grad.val[1, 0, 0])
            print(Domain.v.grad.val[0, 0, 2])

            np.testing.assert_array_almost_equal(Domain.u.grad.val[0, :, :, :-1, :],
                                                 Domain.v.grad.val[0, :, :, :-1, :], decimal=1)
            np.testing.assert_array_almost_equal(Domain.u.grad.val[1, :, :, :, :-1],
                                                 Domain.v.grad.val[1, :, :, :, :-1], decimal=1)


    def test_grad_periodic_wall(self):
        print('\nChecking gradient operator ...')
        for dim, Ni in itertools.product([2,3], [3, 5, 6]):
            Y = [1, 2, 3][:dim]
            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Domain = Domain_PUC(name='new Domain1', N=N, Y=Y)
            for d in np.arange(dim):
                Domain.v = Tensor(name='v', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
                index = dim * [slice(None, None)]
                index[d] = 1
                Domain.v.val[tuple(index)] = 1
                Domain.get_grad_periodic(var=Domain.v)
                # print('v grad \n{}'.format(np.round(Domain.v.grad.val)))
                Domain.u = Tensor(name='u', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
                Domain.get_grad_periodic(var=Domain.u)
                # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                index_posit = copy.deepcopy(index)
                index_posit[d] = slice(0, 1)
                index_negat = copy.deepcopy(index)
                index_negat[d] = slice(1, 2)
                for e in np.arange(Domain.n_epp):
                    Domain.u.grad.val[(d, e, 0, *index_posit)] = 1 / Domain.h[d]
                    # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                    Domain.u.grad.val[(d, e, 0, *index_negat)] = -1 / Domain.h[d]
                    # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                np.testing.assert_allclose(Domain.v.grad.val, Domain.u.grad.val,
                                           err_msg='Gradient test failed for N={} dim={} dir={}'.format(N, dim, d))
                # print()

    def test_D_transpose_bilinear_form(self):
        print('\nChecking transpose gradient operator: (ADu, Dv)=(DTADu, v)...')
        for dim, Ni in itertools.product([2], [4, 5, 6]):
            Y = [1, 2, 3][:dim]
            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            # np.ones(dim, dtype=np.float)
            Domain = Domain_PUC(name='new Domain1', N=N, Y=Y, element_type='bilinear')

            reference_pixel_mat_fun(Domain, kind='identity')
            Domain.apply_quadrature_weights(var=Domain.mat)

            Domain.v = Tensor(name='v', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
            for a in np.arange(dim):
                if dim == 2:
                    Domain.v.val = (30 * Domain.X[0] ** 2) + (40 * Domain.X[
                        1] ** 9)  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a]))#
                if dim == 3:
                    Domain.v.val = (17 * Domain.X[0] ** 2) + (4 * Domain.X[1] ** 6) - (17 * Domain.X[
                        2] ** 8)  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a])) #print('v \n{}'.format(np.round(Domain.v.val, decimals=5)))
                Domain.get_grad_periodic(var=Domain.v)
                # print('v grad \n{}'.format(np.round(Domain.v.grad.val,decimals=5)))

                Domain.u = Tensor(name='u', N=Domain.N, shape=(), Y=Domain.Y, multype='scal')
                if dim == 2:
                    Domain.u.val = (9 * Domain.X[0] ** 2) + (4 * Domain.X[
                        1] ** 2)  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a]))
                if dim == 3:
                    Domain.u.val = (4 * Domain.X[0] ** 3) + (4 * Domain.X[1] ** 5) - (7 * Domain.X[
                        2] ** 2)  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a])) #

                Domain.get_grad_periodic(var=Domain.u)
                # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                # Domain.u.grad.val=Domain.mat * Domain.u.grad

                sum = np.sum(Domain.u.grad.val * Domain.v.grad.val)
                # print('u grad \n{}'.format(Domain.u.grad.val) )
                # print('v grad \n{}'.format(Domain.v.grad.val))
                Domain.u = Domain.get_grad_transpose_periodic(var=Domain.u)
                # print('u \n{}'.format(Domain.u .val))
                # print('v \n{}'.format(Domain.v.val))
                sumT = np.sum(Domain.u.val * Domain.v.val)
                # print('\nChecking transpose gradient operator: (ADu, Dv)=(DTADu, v)...')
                # print(sum)
                # print(sumT)
                self.assertAlmostEqual(sum, sumT, 5)

    def test_laplacian_stencil(self):

        Ns = [3]
        for dim, Ni in itertools.product([2], Ns):

            if dim == 2:
                N = np.array([Ni, Ni +1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Y = N#Ni * np.ones(dim, dtype=np.float)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y, element_type='bilinear')

            reference_pixel_mat_fun(Domain, kind='identity')
            Domain.apply_quadrature_weights(var=Domain.mat)

            ijk = dim * (1,)
            v_1 = Tensor(name='v_1', N=N, shape=(), Y=Y, multype='scal')
            v_1.val[ijk] = 1

            v_1.val[1,0] = 0
            v_1.val[1, 2] = 0

            v_1.val[0, 1] = 0
            v_1.val[2, 1] = 0


            laplace_ijk = copy.deepcopy(Domain.periodic_DAD(v_1))
            if dim == 2:
                stencil_Laplace = np.array([[0., -1., 0., 0],
                                            [-1., 4., -1., 0],
                                            [0., -1., 0., 0]])
            if dim == 3:
                stencil_Laplace = np.array([[[0., 0., 0., 0., 0.],
                                             [0., -1., 0., 0., 0.],
                                             [0., 0., 0., 0., 0.],
                                             [0., 0., 0., 0., 0.]],

                                            [[0., -1., 0., 0., 0.],
                                             [-1., 6., -1., 0., 0.],
                                             [0., -1., 0., 0., 0.],
                                             [0., 0., 0., 0., 0.]],

                                            [[0., 0., 0., 0., 0.],
                                             [0., -1., 0., 0., 0.],
                                             [0., 0., 0., 0., 0.],
                                             [0., 0., 0., 0., 0.]]]
                                           )

            print('FEM DID stencil \n {} '.format(laplace_ijk.val))
            #print('FEM DD stencil \n {} '.format(laplace_ijk_2.val))

           # self.assertTrue(np.allclose(laplace_ijk.val, stencil_Laplace),
            #                msg='Laplacian stencil is not correct in {}'.format(dim))

    def test_elastic_gradient(self):
        print('\nChecking elastic tensor ...')
        for dim, Ni in itertools.product([2, 3], [3, 4, 5]):
            Y = [1, 2, 3][:dim]
            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Domain = Domain_PUC(name='new Domain1', N=N, Y=Y)

            for d in np.arange(dim):
                Domain.v = Tensor(name='v', N=Domain.N, shape=(dim,), Y=Domain.Y, multype='scal')
                index = dim * [slice(None, None)]
                index[d] = 1
                i = [slice(None, None)]
                Domain.v.val[tuple(i + index)] = 1
                Domain.get_grad_periodic(var=Domain.v)
                # print('v grad \n{}'.format(np.round(Domain.v.grad.val)))
                Domain.u = Tensor(name='u', N=Domain.N, shape=(dim,), Y=Domain.Y, multype='scal')
                Domain.get_grad_periodic(var=Domain.u)
                # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                index_posit = copy.deepcopy(index)
                index_posit[d] = slice(0, 1)
                index_negat = copy.deepcopy(index)
                index_negat[d] = slice(1, 2)
                for disp in range(*Domain.u.shape):
                    for e in np.arange(Domain.n_epp):
                        Domain.u.grad.val[(disp, d, e, 0, *index_posit)] = 1 / Domain.h[d]
                        # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                        Domain.u.grad.val[(disp, d, e, 0, *index_negat)] = -1 / Domain.h[d]
                        # print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
                np.testing.assert_allclose(Domain.v.grad.val, Domain.u.grad.val,
                                           err_msg='Gradient test failed for N={} dim={} dir={}'.format(N, dim, d))
                # print()

    def test_elastic_bilinear_form(self):
        print('\nChecking bilineard form for vectro fields: (ADu, Dv)=(DTADu, v)...')
        for dim, Ni in itertools.product([2], [4, 5, 6]):
            Y = [1, 2, 3][:dim]
            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            # np.ones(dim, dtype=np.float)
            Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type='elastic', element_type='linear' )
            # Domain.initialize_mat(kind='elastic')
            mat_0 = 2 * elastic_material_tensor(dim=dim)
            mat_1 = 4 * elastic_material_tensor(dim=dim)
            pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind='circle_inc')

            # reference_pixel_mat_fun(Domain, kind='identity')
            Domain.apply_quadrature_weights(var=Domain.mat)

            Domain.v = Tensor(name='v', N=Domain.N, shape=(dim,), Y=Domain.Y, multype='scal')
            Domain.u = Tensor(name='u', N=Domain.N, shape=(dim,), Y=Domain.Y, multype='scal')

            for disp in range(*Domain.v.shape):
                # for a in np.arange(dim):
                if dim == 2:
                    Domain.v.val[disp] = (disp + 1) * ((30 * Domain.X[0] ** 2) + (40 * Domain.X[
                        1] ** 9))  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a]))#
                    Domain.u.val[disp] = (disp + 1) * ((9 * Domain.X[0] ** 2) + (4 * Domain.X[
                        1] ** 2))
                if dim == 3:
                    Domain.v.val[disp] = (disp + 1) * (
                            (17 * Domain.X[0] ** 2) + (4 * Domain.X[1] ** 6) - (17 * Domain.X[
                        2] ** 8))  # np.where(Domain.X[a]< Y[a]/2, 3*Domain.X[a], -3*(Domain.X[a]- Y[a])) #print('v \n{}'.format(np.round(Domain.v.val, decimals=5)))
                    Domain.u.val[disp] = (disp + 1) * ((4 * Domain.X[0] ** 3) + (4 * Domain.X[1] ** 5) - (7 * Domain.X[
                        2] ** 2))

            Domain.get_grad_periodic(var=Domain.v)
            print('v grad \n{}'.format(np.round(Domain.v.grad.val,decimals=5)))

            Domain.get_grad_periodic(var=Domain.u)
            print('u grad \n{}'.format(np.round(Domain.u.grad.val)))
            Domain.u.grad.val = Domain.mat * Domain.u.grad

            sum = np.sum(Domain.u.grad.val * Domain.v.grad.val)
            # print('u grad \n{}'.format(Domain.u.grad.val))
            # print('v grad \n{}'.format(Domain.v.grad.val))
            Domain.u = Domain.get_grad_transpose_periodic(var=Domain.u)
            # print('u \n{}'.format(Domain.u .val))
            # print('v \n{}'.format(Domain.v.val))
            sumT = np.sum(Domain.u.val * Domain.v.val)
            # print('\nChecking transpose gradient operator: (ADu, Dv)=(DTADu, v)...')
            print(sum)
            print(sumT)
        # self.assertAlmostEqual(sum, sumT, 5)

    def test_system_symmetry(self):
        print('\nChecking symmetry of linear system matrix ...')
        for dim, Ni, problem_type in itertools.product([2, 3], [5, 4], ['conductivity']):#, 'elastic'
            Y = [1, 2, 3][:dim]
            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Domain = Domain_PUC(name='new Domain1', N=N, Y=Y, problem_type=problem_type)
            if problem_type == 'conductivity':
                mat_0 = conductivity_material_tensor(Domain, kind='random_isotropic')
                mat_1 = conductivity_material_tensor(Domain, kind='random_isotropic', rho=15)
            elif problem_type == 'elastic':
                K_0, G_0 = get_bulk_and_shear_modulus(E=210, poison=0.3)

                mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
                mat_1 = mat_0 * 15

            pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind='square_inc')
            Domain.get_ref_mat(mat_type='ones')
            Domain.get_preconditioner(exp_type='kernel')
            Domain.apply_quadrature_weights(var=Domain.mat)
            K = Domain.get_stiffness_matrix()
            MK = Domain.get_Prec_stiffness_matrix()
            M= Domain.get_Prec_matrix()
            MK_d=np.multiply(M.transpose(),K)
            self.assertTrue(check_symmetric(K), msg='Stiffness matrix is NOT symetric')

    def test_system_symmetry_bilinear(self):
        print('\nChecking symmetry of linear system matrix ...')
        for dim, Ni, problem_type in itertools.product([2], [3], ['conductivity']):#, 'elastic'

            if dim == 2:
                N = np.array([Ni, Ni ], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)
            N = [4, 4]
            #Y = [(2*np.pi),(2*np.pi)]
            Y = [9*N[0],12*N[1]]

            Domain = Domain_PUC(name='new Domain1', N=N, Y=Y, problem_type=problem_type, element_type='bilinear')
            if problem_type == 'conductivity':
                mat_0 = np.array([[19/5,0.35, 0][:dim],
                      [0.35, 19, 0][:dim],
                      [0, 0, 5][:dim]][:dim])
                mat_1 = np.array([[1., 0.2, 0][:dim],
                      [0.2, 1., 0][:dim],
                      [0, 0, 5][:dim]][:dim])
            elif problem_type == 'elastic':
                K_0, G_0 = get_bulk_and_shear_modulus(E=210, poison=0.3)

                mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
                mat_1 = mat_0 * 15

            pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_0, kind='square_inc')
            Domain.get_ref_mat(mat_type='ones')
            Domain.get_preconditioner(exp_type='kernel')
            Domain.apply_quadrature_weights(var=Domain.mat)
            K = Domain.get_stiffness_matrix()
            print(K)
            MK = Domain.get_Prec_stiffness_matrix()
            M= Domain.get_Prec_matrix()
            MK_d=np.multiply(M.transpose(),K)
            self.assertTrue(check_symmetric(K), msg='Stiffness matrix is NOT symetric')

    def t_est_stiffness_tensor(self):
        dim = 2
        strains = np.identity(dim)
        counteris = 1
        rhos = [1]  # [10, 1e2, 1e3]
        Ns = [3]  # ,6,9, 12, 15, 18]  # [8,9,10,11] #
        i = 0
        for dim, Ni, rho in itertools.product([dim], Ns, rhos):
            Y = np.ones(dim, dtype=np.float)
            N = np.array(dim * [Ni, ], dtype=np.int)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type='elastic')

            E = 210 * 1e9
            poison = 0.2

            K = E / (3 * (1 - 2 * poison))
            G = E / (2 * (1 + poison))

            mat_0 = elastic_material_tensor(dim=dim, K=K, mu=G)
            mat_V = compute_Vight_notation(mat_0)
            print(np.round(mat_V, decimals=10))

            pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_0, kind='constant')
            # print(mat_0)

            v_1 = Tensor(name='v_1', N=N, shape=Domain.u_shape, Y=Y, multype='scal')
            v_1.val[(0,) + (1, 1, 1)[:dim]] = 1

            Domain.get_grad_periodic(var=v_1)
            v_2 = copy.deepcopy(v_1)

            v_1.grad.val = Domain.mat * v_1.grad

            # print(v_1.grad.val-v_2.grad.val)
            print('grad u1')
            print(v_2.grad.val[0])
            print(v_1.grad.val[0])

            print('grad u2')
            print(v_2.grad.val[1])
            print(v_1.grad.val[1])

        # print()
        # print(v_2.grad.val[2])
        # print(v_1.grad.val[2])

        # print(np.equal(v_1.grad.val[0], v_2.grad.val[0]))

        # print(np.equal(v_1.grad.val[1], v_2.grad.val[1]).all())
        # print(np.equal(v_1.grad.val[2], v_2.grad.val[2]).all())

    def t_est_DAD_stencil(self):

        rhos = [2]  # [10, 1e2, 1e3]
        Ns = [4]  # ,6,9, 12, 15, 18]  # [8,9,10,11] #
        for dim, Ni, rho in itertools.product([2], Ns, rhos):

            if dim == 2:
                N = np.array([Ni, Ni + 1], dtype=np.int)
            if dim == 3:
                N = np.array([Ni, Ni + 1, Ni + 2], dtype=np.int)

            Y = N  # Ni * np.ones(dim, dtype=np.float)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y)

            reference_pixel_mat_fun(Domain, kind='discontinuous_test', rho=rho)

            Domain.get_ref_mat()
            Domain.get_preconditioner()
            Domain.apply_quadrature_weights(var=Domain.mat)

            # assembly of stiffness matrix
            DDijk = np.zeros((Ni ** dim, *N))  # Laplacian matrix
            for ijk in iter_product(N):
                # ijk = dim * (i,)
                v_1 = Tensor(name='v_1', N=N, shape=(), Y=Y, multype='scal')
                v_1.val[ijk] = 1
                Pv_1 = copy.deepcopy(v_1)
                # grad_test=copy.deepcopy(Domain.get_grad_periodic(v_1))

                elliptic_stencil = copy.deepcopy(Domain.lhs_periodic(v_1))
                Pelliptic_stencil = copy.deepcopy(Domain.prec_lhs_periodic(Pv_1))
                if dim == 2:
                    stencil_ = np.array([[0., -1.1, 0.1], [-1.1, 4.2, -1.1], [0.1, -1.1, 0.]])
                if dim == 3:
                    stencil_ = np.array([[[0., 0., 0., ],
                                          [0., -0.33333333, 0., ],
                                          [0., 0., 0., ]],

                                         [[0., -0.33333333, 0.],
                                          [-0.33333333, 2., -0.33333333],
                                          [0., -0.33333333, 0., ]],

                                         [[0., 0., 0.],
                                          [0., -0.33333333, 0.],
                                          [0., 0., 0.]]]
                                        )

                print('FEM stencil \n {} '.format(elliptic_stencil.val))
                print('Prec FEM stencil \n {} '.format(np.round(Pelliptic_stencil.val)))
            #print(stencil_)

    def t_est_material(self):

        rhos = [1e4]  # [10, 1e2, 1e3]

        Ns = [
            30]  # np.arange(5,6,2)#[3, 3**2,3**3,3**4]  # [3,6,9,12,15] # [3,,9,12,15,18] ,1e4,1e5,1e6,1e7,1e8,1e9]#,9,18,27]#np.arange(8, 25,2)
        for dim, Ni, rho in itertools.product([2], Ns, rhos):
            Y = np.ones(dim, dtype=np.float)
            N = np.array(dim * [Ni, ], dtype=np.int)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y)

            mat_0 = np.array([[10., 2.], [2., 5.]])
            mat_1 = np.array([[1., 0.], [0., 2.]])
            x0 = Domain.get_x0()
            pixel_mat_fun(Domain, mat_0=mat_0, mat_1=mat_1, kind='circle_inc_qp')

            # Domain.mat.val = mat_fun(Domain.X_qp, dim=dim, rho=rho, kind='circle_inc')  # always el. wise constant

            plt.imshow(Domain.mat.val[0, 0, 0, 0])
            plt.show()
            plt.imshow(Domain.mat.val[0, 0, 1, 0])
            plt.show()
            ## TODO prepare material showcase
            mat_geom = 'circle_inc'
            problem_type = 'elastic'
            Y = np.ones(dim, dtype=np.float)
            N = np.array(dim * [Ni, ], dtype=np.int)

            Domain = Domain_PUC(name='Domain', N=N, Y=Y, problem_type=problem_type)

            # mat_0 = material(Domain,kind='random')

            # mat_1 =  material(Domain,kind='random',rho=rho)
            # mat_0 = conductivity_material_tensor(dom=Domain, kind='ip_mat_0')
            # mat_1 = conductivity_material_tensor(dom=Domain, kind='ip_mat_1') * rho
            E_0 = 210
            E_1 = 210 * rho
            poison = 0.2

            K_0 = E_0 / (3 * (1 - 2 * poison))
            G_0 = E_0 / (2 * (1 + poison))

            K_1 = E_1 / (3 * (1 - 2 * poison))
            G_1 = E_1 / (2 * (1 + poison))

            mat_0 = elastic_material_tensor(dim=dim, K=K_0, mu=G_0)
            mat_0_V = compute_Vight_notation(mat_0)
            # print(mat_0_V)
            # print(np.linalg.eigvals(mat_0_V))
            mat_1 = elastic_material_tensor(dim=dim, K=K_1, mu=G_1)
            mat_1_V = compute_Vight_notation(mat_1)


if __name__ == '__main__':
    print()
