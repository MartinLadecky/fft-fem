#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   plot_functions.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  Plot functions adjusted for tensor.py fields

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""


import numpy as np
import pickle
import itertools

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mticker



def plot_field(coord, val, dim):
    if dim == 1:
        fig = plt.figure()
        plt.plot(coord[0], val)

    if dim == 2:
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        # Plot the surface.
        surf = ax.plot_surface(coord[0], coord[1], val, cmap=cm.coolwarm,
                               linewidth=0.2, antialiased=False, edgecolors='none', alpha=0.4)
        # surf = ax.scatter(coord[0], coord[1], val)

        ax.set_xlabel('x[0] direction')
        ax.set_ylabel('x[1] direction')

    if dim == 3:
        multi_slice_viewer(val[:, :, :])  # x direction is stable,
        print('Plot 3D: switch slice in x-direction by j,k key')
    return


def remove_keymap_conflicts(new_keys_set):
    for prop in plt.rcParams:
        if prop.startswith('keymap.'):
            keys = plt.rcParams[prop]
            remove_list = set(keys) & new_keys_set
            for key in remove_list:
                keys.remove(key)


def scy_text(a):
    f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
    g = lambda x, pos: "${}$".format(f._formatSciNotation('%1.10e' % x))
    fmt = mticker.FuncFormatter(g)
    return fmt(a)


def multi_slice_viewer(volume):
    remove_keymap_conflicts({'j', 'k'})
    fig, ax = plt.subplots()
    ax.volume = volume
    ax.index = volume.shape[0] // 2
    ax.imshow(volume[ax.index])
    fig.canvas.mpl_connect('key_press_event', process_key)


def process_key(event):
    fig = event.canvas.figure
    ax = fig.axes[0]
    if event.key == 'j':
        previous_slice(ax)
    elif event.key == 'k':
        next_slice(ax)
    fig.canvas.draw()


def previous_slice(ax):
    volume = ax.volume
    ax.index = (ax.index - 1) % volume.shape[0]  # wrap around using %
    ax.images[0].set_array(volume[ax.index])


def next_slice(ax):
    volume = ax.volume
    ax.index = (ax.index + 1) % volume.shape[0]
    ax.images[0].set_array(volume[ax.index])


def plot_convergence_rate(dim, Ni):
    rhos = pickle.load(open("./experiments/exp_data/rhos_dim{}Ni{}.p".format(dim, Ni), "rb"))
    num_it_PCG = pickle.load(open("./experiments/exp_data/num_it_PCG_dim{}Ni{}.p".format(dim, Ni), "rb"))
    num_it_CG = pickle.load(open("./experiments/exp_data/num_it_CG_dim{}Ni{}.p".format(dim, Ni), "rb"))

    parf = set_pars(mpl)
    src = '../experiments/figures/'  # source folder\
    plt.figure(num=None, figsize=parf['figsize'], dpi=parf['dpi'])
    plt.ylabel('Residual norm')
    plt.xlabel('Iteration')

    f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
    g = lambda x, pos: "${}$".format(f._formatSciNotation('%1.10e' % x))
    fmt = mticker.FuncFormatter(g)

    # for rho in rhos:
    # info_PCG = pickle.load(open("./experiments/exp_data/info_PCG_dim{}Ni{}rho{}.p".format(dim, Ni,int(rho)), "rb"))
    # info_CG = pickle.load(open("./experiments/exp_data/info_CG_dim{}Ni{}rho{}.p".format(dim, Ni,int(rho)), "rb"))
    # plt.semilogy(np.arange(info_PCG['kit'] + 1), info_PCG['res_log'], label=r'$\rho$={}'.format(fmt(rho)))
    # plt.semilogy(np.arange(info_CG['kit'] + 1), info_CG['res_log'], label='rho={}'.format(rho))

    plt.title(r'Convergence rate w.r. to $\rho$')
    plt.legend()

    plt.legend(loc='best')
    fname = src + 'Convergence_rate_dim{}_N{}{}'.format(dim, Ni, '.pdf')
    print(('create figure: {}'.format(fname)))
    plt.savefig(fname, dpi=parf['dpi'], pad_inches=parf['pad_inches'], bbox_inches='tight')
    print('END plot convergence rate')

    plt.figure(num=None, figsize=parf['figsize'], dpi=parf['dpi'])
    plt.ylabel('Num. of iter. / 1e-6 res. norm')
    plt.xlabel(r'Phase contrast $\rho$')
    f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
    g = lambda x, pos: "${}$".format(f._formatSciNotation('%1.10e' % x))
    fmt = mticker.FuncFormatter(g)
    markers = itertools.cycle((',', '+', 'x', '.', 'o', '*'))
    # plt.loglog(rhos, num_it_CG, '--', label="CG")
    for Ni in [12, 18, 27]:
        rhos = pickle.load(open("./experiments/exp_data/rhos_dim{}Ni{}.p".format(dim, Ni), "rb"))
        num_it_PCG = pickle.load(open("./experiments/exp_data/num_it_PCG_dim{}Ni{}.p".format(dim, Ni), "rb"))

        plt.loglog(rhos, num_it_PCG, label="PCG- N={}".format(Ni), linewidth=0.5, marker=next(markers),
                   markeredgewidth=1, markerfacecolor='None')

    plt.loglog(rhos, rhos, '--', color='grey', label=r"$\rho$", linewidth=0.5)
    plt.loglog(rhos, np.sqrt(rhos), '-.', color='grey', label=r"$\sqrt\rho$", linewidth=0.5)
    ax = plt.gca()
    ax.set_xlim([0, rhos[-1]])
    ax.set_ylim([0, rhos[-1]])

    plt.title(r'Convergence rate ')
    plt.legend()

    plt.legend(loc='best')
    fname = src + 'Convergence_rate2_dim{}_N{}{}'.format(dim, Ni, '.pdf')
    print(('create figure: {}'.format(fname)))
    plt.savefig(fname, dpi=parf['dpi'], pad_inches=parf['pad_inches'], bbox_inches='tight')
    print('END plot convergence rate2')


def copy_files(src, dest, files='all'):
    import os
    from shutil import copy
    src_files = os.listdir(src)
    for file_name in src_files:
        if files == 'all' or file_name in files:
            full_file_name = os.path.join(src, file_name)
            if (os.path.isfile(full_file_name)):
                copy(full_file_name, dest)
        else:
            continue
    print('copy of files is finished')
    return


def set_pars(mpl):
    mpl.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath,bm,amsfonts}"]
    params = {'text.usetex': True,
              'font.family': 'serif',
              'font.size': 12,
              'legend.fontsize': 10,
              }
    mpl.rcParams.update(params)
    fig_par = {'dpi': 1000,
               'facecolor': 'w',
               'edgecolor': 'k',
               'figsize': (4, 3),
               'figsize3D': (4, 4),
               'pad_inches': 0.02,
               }

    return fig_par
