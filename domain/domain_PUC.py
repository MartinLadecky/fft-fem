#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   domain_PUC.py

@author Martin Ladecký <m.ladecky@gmail.com>

@date   1 Feb 2020

@brief  Periodic Unit Cell domain object

Copyright © 2020 Martin Ladecký

µFFT-FEM is free software; you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µFFT-FEM is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with µFFT-FEM; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import numpy as np
import copy

from domain import iter_product
from tensors.tensor import Tensor


class Domain_PUC():
    def __init__(self, name='', N=None, Y=None, problem_type='conductivity', element_type='linear'):

        self.name = name
        self.N = tuple(np.array(N, dtype=np.int))  # number of pixels, non-periodic nodes
        self.dim = self.N.__len__()
        self.element_type = element_type

        if self.dim == 3 and self.element_type == 'bilinear':
            raise ValueError('I do NOT have 3D bilinear Elements')
        if not problem_type in ['conductivity', 'elastic']:
            raise ValueError('Unrecognised problem_type')

        if problem_type == 'conductivity':
            self.u_shape = ()
            self.du_shape = (self.dim,)
            self.mat_shape = 2 * (self.dim,)
        elif problem_type == 'elastic':
            self.u_shape = (self.dim,)
            self.du_shape = 2 * (self.dim,)
            self.mat_shape = 4 * (self.dim,)

        if Y is None:
            self.Y = np.ones(self.dim, dtype=np.float)
        else:
            self.Y = np.array(Y)
        self.h = Y / (np.array(N, dtype=np.int))

        if element_type == 'linear':
            if self.dim == 2:
                self.n_epp = 2
                self.n_qp = 1
            elif self.dim == 3:
                self.n_epp = 6
                self.n_qp = 1
        if element_type == 'bilinear':
            if self.dim == 2:
                self.n_epp = 1
                self.n_qp = 4
            elif self.dim == 3:
                self.n_epp = 1
                self.n_qp = 8

        self.set_nodal_coord()
        self.set_quad_coord()
        self.initialize_mat(kind=problem_type)
        print()

    def get_grad_periodic(self, var):
        if var is None:
            raise ValueError('I do NOT have variable " {} ".'.format(var.name))

        if self.element_type == 'linear':
            if self.dim == 2:
                if not hasattr(var, 'grad'):
                    var.grad = Tensor(name=' grad {} '.format(var.name), N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp],
                                      Y=self.Y,
                                      shape=[*var.shape, self.dim],
                                      multype='vec')  ## TODO multype 'vec' does not exist
                if var.shape == ():
                    for point_index in iter_product(self.N):
                        index = np.array(point_index)
                        for dir in np.arange(self.dim):
                            index_p = np.array((0,) * self.dim)
                            index_p[dir] = 1
                            point_index_plus = tuple(
                                (index + index_p) % self.N)  # tuple(map(sum, zip(point_index, index_p)))
                            point_index_minus = tuple(
                                (index - index_p) % self.N)  # tuple(map(sum, zip(point_index, index_m)))

                            var.grad.val[(dir, 0, 0, *index)] = (var.val[point_index_plus] - var.val[point_index]) / \
                                                                self.h[
                                                                    dir]
                            var.grad.val[(dir, 1, 0, *(tuple(index - 1)))] = (-var.val[point_index_minus] + var.val[
                                point_index]) / self.h[dir]
                else:
                    for disp in range(*var.shape):
                        for point_index in iter_product(self.N):
                            index = np.array(point_index)
                            for dir in np.arange(self.dim):
                                index_p = np.array((0,) * self.dim)
                                index_p[dir] = 1
                                point_index_plus = tuple(
                                    (index + index_p) % self.N)  # tuple(map(sum, zip(point_index, index_p)))
                                point_index_minus = tuple(
                                    (index - index_p) % self.N)  # tuple(map(sum, zip(point_index, index_m)))

                                var.grad.val[(disp, dir, 0, 0, *index)] = (var.val[(disp,) + point_index_plus] -
                                                                           var.val[
                                                                               (disp,) + point_index]) / self.h[
                                                                              dir]
                                var.grad.val[(disp, dir, 1, 0, *(tuple(index - 1)))] = (-var.val[
                                    (disp,) + point_index_minus] +
                                                                                        var.val[(disp,) +
                                                                                                point_index]) / self.h[
                                                                                           dir]

            elif self.dim == 3:
                if not hasattr(var, 'grad'):
                    var.grad = Tensor(name=' grad {} '.format(var.name), N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp],
                                      Y=self.Y,
                                      shape=[*var.shape, self.dim],
                                      multype='vec')
                if var.shape == ():
                    for point_index in iter_product(self.N):
                        index = np.array(point_index)
                        for dir in np.arange(self.dim):
                            index_p = np.array((0,) * self.dim)
                            index_p[dir] = 1
                            point_index_plus = tuple(
                                (index + index_p) % self.N)
                            point_index_minus = tuple(
                                (index - index_p) % self.N)
                            grad_forward = (var.val[point_index_plus] - var.val[point_index]) / self.h[dir]
                            grad_backward = (-var.val[point_index_minus] + var.val[point_index]) / self.h[dir]
                            if dir == 0:
                                # print(index)
                                index_0 = tuple(index)
                                index_2 = tuple((index + [0, 0, -1]) % np.array(self.N))
                                index_4 = tuple((index + [0, 0, -1]) % np.array(self.N))

                                var.grad.val[(dir, 0, 0, *index_0)] = grad_forward
                                var.grad.val[(dir, 2, 0, *index_2)] = grad_forward
                                var.grad.val[(dir, 4, 0, *index_4)] = grad_forward
                                # print(var.grad.val[0, 0, 0, :, :, 4])
                                index_1 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                index_3 = tuple((index + [-1, -1, 0]) % np.array(self.N))
                                index_5 = tuple((index + [-1, -1, 0]) % np.array(self.N))

                                var.grad.val[(dir, 1, 0, *index_1)] = grad_backward
                                var.grad.val[(dir, 3, 0, *index_3)] = grad_backward
                                var.grad.val[(dir, 5, 0, *index_5)] = grad_backward
                                # print(var.grad.val[0, 0, 0, :, :, 4])
                            elif dir == 1:
                                index_0 = tuple(index)
                                index_2 = tuple(index)
                                index_5 = tuple((index + [-1, 0, 0]) % np.array(self.N))

                                var.grad.val[(dir, 0, 0, *index_0)] = grad_forward
                                var.grad.val[(dir, 2, 0, *index_2)] = grad_forward
                                var.grad.val[(dir, 5, 0, *index_5)] = grad_forward

                                index_1 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                index_3 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                index_4 = tuple((index + [0, -1, -1]) % np.array(self.N))

                                var.grad.val[(dir, 1, 0, *index_1)] = grad_backward
                                var.grad.val[(dir, 3, 0, *index_3)] = grad_backward
                                var.grad.val[(dir, 4, 0, *index_4)] = grad_backward
                            elif dir == 2:
                                index_0 = tuple((index + [-1, 0, 0]) % np.array(self.N))
                                index_2 = tuple(index)
                                index_5 = tuple((index + [-1, 0, 0]) % np.array(self.N))

                                var.grad.val[(dir, 0, 0, *index_0)] = grad_forward
                                var.grad.val[(dir, 2, 0, *index_2)] = grad_forward
                                var.grad.val[(dir, 5, 0, *index_5)] = grad_forward

                                index_1 = tuple((index + [0, -1, -1]) % np.array(self.N))
                                index_3 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                index_4 = tuple((index + [0, -1, -1]) % np.array(self.N))

                                var.grad.val[(dir, 1, 0, *index_1)] = grad_backward
                                var.grad.val[(dir, 3, 0, *index_3)] = grad_backward
                                var.grad.val[(dir, 4, 0, *index_4)] = grad_backward

                else:
                    for disp in range(*var.shape):
                        for point_index in iter_product(self.N):
                            index = np.array(point_index)
                            for dir in np.arange(self.dim):
                                index_p = np.array((0,) * self.dim)
                                index_p[dir] = 1
                                point_index_plus = tuple(
                                    (index + index_p) % self.N)
                                point_index_minus = tuple(
                                    (index - index_p) % self.N)
                                grad_forward = (var.val[(disp,) + point_index_plus] - var.val[(disp,) + point_index]) / \
                                               self.h[dir]
                                grad_backward = (-var.val[(disp,) + point_index_minus] + var.val[
                                    (disp,) + point_index]) / \
                                                self.h[dir]
                                # TODO: try just forward
                                if dir == 0:
                                    # print(index)
                                    index_0 = tuple(index)
                                    index_2 = tuple((index + [0, 0, -1]) % np.array(self.N))
                                    index_4 = tuple((index + [0, 0, -1]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 0, 0, *index_0)] = grad_forward
                                    var.grad.val[(disp, dir, 2, 0, *index_2)] = grad_forward
                                    var.grad.val[(disp, dir, 4, 0, *index_4)] = grad_forward

                                    index_1 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                    index_3 = tuple((index + [-1, -1, 0]) % np.array(self.N))
                                    index_5 = tuple((index + [-1, -1, 0]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 1, 0, *index_1)] = grad_backward
                                    var.grad.val[(disp, dir, 3, 0, *index_3)] = grad_backward
                                    var.grad.val[(disp, dir, 5, 0, *index_5)] = grad_backward
                                    # print(var.grad.val[0, 0, 0, :, :, 4])
                                elif dir == 1:
                                    index_0 = tuple(index)
                                    index_2 = tuple(index)
                                    index_5 = tuple((index + [-1, 0, 0]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 0, 0, *index_0)] = grad_forward
                                    var.grad.val[(disp, dir, 2, 0, *index_2)] = grad_forward
                                    var.grad.val[(disp, dir, 5, 0, *index_5)] = grad_forward

                                    index_1 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                    index_3 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                    index_4 = tuple((index + [0, -1, -1]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 1, 0, *index_1)] = grad_backward
                                    var.grad.val[(disp, dir, 3, 0, *index_3)] = grad_backward
                                    var.grad.val[(disp, dir, 4, 0, *index_4)] = grad_backward
                                elif dir == 2:
                                    index_0 = tuple((index + [-1, 0, 0]) % np.array(self.N))
                                    index_2 = tuple(index)
                                    index_5 = tuple((index + [-1, 0, 0]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 0, 0, *index_0)] = grad_forward
                                    var.grad.val[(disp, dir, 2, 0, *index_2)] = grad_forward
                                    var.grad.val[(disp, dir, 5, 0, *index_5)] = grad_forward

                                    index_1 = tuple((index + [0, -1, -1]) % np.array(self.N))
                                    index_3 = tuple((index + [-1, -1, -1]) % np.array(self.N))
                                    index_4 = tuple((index + [0, -1, -1]) % np.array(self.N))

                                    var.grad.val[(disp, dir, 1, 0, *index_1)] = grad_backward
                                    var.grad.val[(disp, dir, 3, 0, *index_3)] = grad_backward
                                    var.grad.val[(disp, dir, 4, 0, *index_4)] = grad_backward

        if self.element_type == 'bilinear':
            if self.dim == 2:

                if not hasattr(var, 'grad'):
                    var.grad = Tensor(name=' grad {} '.format(var.name), N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp],
                                      Y=self.Y,
                                      shape=[*var.shape, self.dim],
                                      multype='vec')  ## TODO multype 'vec' does not exist
                delxy = (2 * self.h[0] * self.h[1])
                delx = self.h[0]
                dely = self.h[1]
                if var.shape == ():
                    crd_0=-1. / np.sqrt(3)
                    crd_1 = 1. / np.sqrt(3)
                    quad_coord=np.zeros([4,2])
                    quad_coord[0]=[crd_0, crd_0]
                    quad_coord[1] = [crd_1, crd_0]
                    quad_coord[2] = [crd_0, crd_1]
                    quad_coord[3] = [crd_1, crd_1]
                    self.B = np.zeros([8, 4])
                    for q in np.arange(self.n_qp):
                        self.B[2 * q, 0] = (( ( quad_coord[q,1] ) - 1.) * dely / delxy)
                        self.B[2 * q, 1] = ((-( quad_coord[q,1] ) + 1.) * dely / delxy)
                        self.B[2 * q, 2] = ((-( quad_coord[q,1] ) - 1.) * dely / delxy)
                        self.B[2 * q, 3] = (( ( quad_coord[q,1] ) + 1.) * dely / delxy)

                        self.B[2 * q + 1, 0] = (( (quad_coord[q,0]) - 1.) * delx / delxy)
                        self.B[2 * q + 1, 1] = ((-(quad_coord[q,0]) - 1.) * delx / delxy)
                        self.B[2 * q + 1, 2] = ((-(quad_coord[q,0]) + 1.) * delx / delxy)
                        self.B[2 * q + 1, 3] = (( (quad_coord[q,0]) + 1.) * delx / delxy)

                    self.Btran = self.B.transpose()

                    for point_index in iter_product(self.N):
                        index = np.array(point_index)
                        point_index_0 = tuple(
                            (index + [0, 0]) % self.N)
                        point_index_1 = tuple(
                            (index + [1, 0]) % self.N)
                        point_index_2 = tuple(
                            (index + [0, 1]) % self.N)
                        point_index_3 = tuple(
                            (index + [1, 1]) % self.N)
                        for q in np.arange(self.n_qp):
                            var.grad.val[(0, 0, q, *index)] = ((var.val[point_index_0] * self.B[2 * q, 0]) + \
                                                               (var.val[point_index_1] * self.B[2 * q, 1]) + \
                                                               (var.val[point_index_2] * self.B[2 * q, 2]) + \
                                                               (var.val[point_index_3] * self.B[2 * q, 3]))

                            var.grad.val[(1, 0, q, *index)] = ((var.val[point_index_0]* self.B[2 * q +1 , 0]) + \
                                                               (var.val[point_index_1]* self.B[2 * q +1 , 1])  + \
                                                               (var.val[point_index_2]* self.B[2 * q +1 , 2])+ \
                                                               (var.val[point_index_3]* self.B[2 * q +1 , 3]))

        return var

    def get_grad_transpose_periodic(self, var):
        if not hasattr(var, 'grad'):
            raise ValueError('Variable " {} " does NOT have grad.'.format(var.name))

        if self.element_type == 'linear':
            if self.dim == 2:
                if var.shape == ():
                    for ki, kj in iter_product(self.N):
                        var.val[ki, kj] = ((var.grad.val[0, 1, 0, ki - 1, kj - 1] - var.grad.val[0, 1, 0, ki, kj - 1] +
                                            var.grad.val[0, 0, 0, ki - 1, kj] - var.grad.val[0, 0, 0, ki, kj]) / self.h[
                                               0]) + \
                                          ((var.grad.val[1, 1, 0, ki - 1, kj - 1] - var.grad.val[1, 1, 0, ki - 1, kj] +
                                            var.grad.val[1, 0, 0, ki, kj - 1] - var.grad.val[1, 0, 0, ki, kj]) / self.h[
                                               1])
                else:
                    for disp in range(*var.shape):
                        for ki, kj in iter_product(self.N):
                            var.val[disp, ki, kj] = ((var.grad.val[disp, 0, 1, 0, ki - 1, kj - 1] - var.grad.val[
                                disp, 0, 1, 0, ki, kj - 1] +
                                                      var.grad.val[disp, 0, 0, 0, ki - 1, kj] - var.grad.val[
                                                          disp, 0, 0, 0, ki, kj]) / self.h[0]) + \
                                                    ((var.grad.val[disp, 1, 1, 0, ki - 1, kj - 1] - var.grad.val[
                                                        disp, 1, 1, 0, ki - 1, kj] +
                                                      var.grad.val[disp, 1, 0, 0, ki, kj - 1] - var.grad.val[
                                                          disp, 1, 0, 0, ki, kj]) / self.h[
                                                         1])
                    # print()

            if self.dim == 3:
                if var.shape == ():
                    var.val = np.zeros(var.val.shape)
                    for point_index in iter_product(self.N):
                        # index = np.array(point_index)
                        i, j, k = point_index
                        # print(i,j,k)

                        for dir in np.arange(self.dim):
                            if dir == 0:
                                # print(( dir, 0, 0, i - 1, j, k))
                                var.val[point_index] += ((var.grad.val[(dir, 0, 0, i - 1, j, k)] -
                                                          var.grad.val[(dir, 0, 0, i, j, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 1, 0, i - 1, j - 1, k - 1)] - var.grad.val[
                                    (dir, 1, 0, i, j - 1, k - 1)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 2, 0, i - 1, j, k - 1)] - var.grad.val[
                                    (dir, 2, 0, i, j, k - 1)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 3, 0, i - 1, j - 1, k)] - var.grad.val[
                                    (dir, 3, 0, i, j - 1, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 4, 0, i - 1, j, k - 1)] - var.grad.val[
                                    (dir, 4, 0, i, j, k - 1)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 5, 0, i - 1, j - 1, k)] - var.grad.val[
                                    (dir, 5, 0, i, j - 1, k)]) / self.h[dir])
                                # print(var.val)
                            elif dir == 1:

                                var.val[point_index] += ((var.grad.val[(dir, 0, 0, i, j - 1, k)] -
                                                          var.grad.val[(dir, 0, 0, i, j, k)]) / self.h[dir])

                                var.val[point_index] += ((var.grad.val[(dir, 1, 0, i - 1, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 1, 0, i - 1, j, k - 1)]) / self.h[dir])

                                var.val[point_index] += ((var.grad.val[(dir, 2, 0, i, j - 1, k)] -
                                                          var.grad.val[(dir, 2, 0, i, j, k)]) / self.h[dir])

                                var.val[point_index] += ((var.grad.val[(dir, 3, 0, i - 1, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 3, 0, i - 1, j, k - 1)]) / self.h[dir])

                                var.val[point_index] += ((var.grad.val[(dir, 4, 0, i, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 4, 0, i, j, k - 1)]) / self.h[dir])

                                var.val[point_index] += ((var.grad.val[(dir, 5, 0, i - 1, j - 1, k)] -
                                                          var.grad.val[(dir, 5, 0, i - 1, j, k)]) / self.h[dir])
                                # print(var.val)


                            elif dir == 2:

                                var.val[point_index] += ((var.grad.val[(dir, 0, 0, i - 1, j, k - 1)] -
                                                          var.grad.val[(dir, 0, 0, i - 1, j, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 1, 0, i, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 1, 0, i, j - 1, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 2, 0, i, j, k - 1)] -
                                                          var.grad.val[(dir, 2, 0, i, j, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 3, 0, i - 1, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 3, 0, i - 1, j - 1, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 4, 0, i, j - 1, k - 1)] -
                                                          var.grad.val[(dir, 4, 0, i, j - 1, k)]) / self.h[dir])
                                var.val[point_index] += ((var.grad.val[(dir, 5, 0, i - 1, j, k - 1)] -
                                                          var.grad.val[(dir, 5, 0, i - 1, j, k)]) / self.h[dir])

                else:
                    var.val = np.zeros(var.val.shape)
                    for disp in range(*var.shape):
                        for point_index in iter_product(self.N):
                            # index = np.array(point_index)
                            i, j, k = point_index
                            # print(i,j,k)

                            for dir in np.arange(self.dim):
                                if dir == 0:
                                    # print(var.grad.val[(disp,dir, 0, 0, i - 1, j, k)])
                                    # print(var.grad.val[(disp, dir, 0, 0, i, j, k)])
                                    # print(var.val[disp, point_index])
                                    var.val[(disp,) + point_index] += ((var.grad.val[(disp, dir, 0, 0, i - 1, j, k)] -
                                                                        var.grad.val[(disp, dir, 0, 0, i, j, k)]) /
                                                                       self.h[
                                                                           dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 1, 0, i - 1, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 1, 0, i, j - 1, k - 1)]) / self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 2, 0, i - 1, j, k - 1)] -
                                             var.grad.val[(disp, dir, 2, 0, i, j, k - 1)]) /
                                            self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 3, 0, i - 1, j - 1, k)] -
                                             var.grad.val[(disp, dir, 3, 0, i, j - 1, k)]) /
                                            self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 4, 0, i - 1, j, k - 1)] -
                                             var.grad.val[(disp, dir, 4, 0, i, j, k - 1)]) /
                                            self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 5, 0, i - 1, j - 1, k)] -
                                             var.grad.val[(disp, dir, 5, 0, i, j - 1, k)]) /
                                            self.h[dir])
                                    # print(var.val)
                                elif dir == 1:

                                    var.val[(disp,) + point_index] += ((var.grad.val[(disp, dir, 0, 0, i, j - 1, k)] -
                                                                        var.grad.val[(disp, dir, 0, 0, i, j, k)]) /
                                                                       self.h[
                                                                           dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 1, 0, i - 1, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 1, 0, i - 1, j, k - 1)]) / self.h[dir])

                                    var.val[(disp,) + point_index] += ((var.grad.val[(disp, dir, 2, 0, i, j - 1, k)] -
                                                                        var.grad.val[(disp, dir, 2, 0, i, j, k)]) /
                                                                       self.h[
                                                                           dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 3, 0, i - 1, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 3, 0, i - 1, j, k - 1)]) / self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 4, 0, i, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 4, 0, i, j, k - 1)]) /
                                            self.h[dir])

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 5, 0, i - 1, j - 1, k)] -
                                             var.grad.val[(disp, dir, 5, 0, i - 1, j, k)]) /
                                            self.h[dir])
                                    # print(var.val)


                                elif dir == 2:

                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 0, 0, i - 1, j, k - 1)] -
                                             var.grad.val[(disp, dir, 0, 0, i - 1, j, k)]) /
                                            self.h[dir])
                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 1, 0, i, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 1, 0, i, j - 1, k)]) /
                                            self.h[dir])
                                    var.val[(disp,) + point_index] += ((var.grad.val[(disp, dir, 2, 0, i, j, k - 1)] -
                                                                        var.grad.val[(disp, dir, 2, 0, i, j, k)]) /
                                                                       self.h[
                                                                           dir])
                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 3, 0, i - 1, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 3, 0, i - 1, j - 1, k)]) / self.h[dir])
                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 4, 0, i, j - 1, k - 1)] -
                                             var.grad.val[(disp, dir, 4, 0, i, j - 1, k)]) /
                                            self.h[dir])
                                    var.val[(disp,) + point_index] += (
                                            (var.grad.val[(disp, dir, 5, 0, i - 1, j, k - 1)] -
                                             var.grad.val[(disp, dir, 5, 0, i - 1, j, k)]) /
                                            self.h[dir])

        if self.element_type == 'bilinear':
            if self.dim == 2:
                var.val = np.zeros(var.val.shape)
                delxy = (2 * self.h[0] * self.h[1])
                delx = self.h[0]
                dely = self.h[1]
                if var.shape == ():

                    for point_index in iter_product(self.N):
                        index = np.array(point_index)
                        point_index_0 = tuple(
                            (index + [0, 0]) % self.N)
                        point_index_1 = tuple(
                            (index + [1, 0]) % self.N)
                        point_index_2 = tuple(
                            (index + [0, 1]) % self.N)
                        point_index_3 = tuple(
                            (index + [1, 1]) % self.N)
                        for q in np.arange(self.n_qp):
                            var.val[point_index_0] +=  self.Btran[0,2*q] * var.grad.val[(0, 0, q,*index)] + \
                                                       self.Btran[0,2*q+1] * var.grad.val[(1, 0, q, *index)]

                            var.val[point_index_1] +=  self.Btran[1,2*q] * var.grad.val[(0, 0, q,*index)] + \
                                                       self.Btran[1,2*q+1] * var.grad.val[(1, 0, q, *index)]

                            var.val[point_index_2] +=  self.Btran[2,2*q] * var.grad.val[(0, 0, q,*index)] + \
                                                       self.Btran[2,2*q+1] * var.grad.val[(1, 0, q, *index)]

                            var.val[point_index_3] +=  self.Btran[3,2*q] * var.grad.val[(0, 0, q,*index)] + \
                                                       self.Btran[3,2*q+1] * var.grad.val[(1, 0, q, *index)]

        return var

    def set_nodal_coord(self):
        self.X = np.meshgrid(*[np.linspace(0, self.Y[d] - self.h[d], self.N[d]) for d in range(0, self.dim)],
                             indexing='ij')

    def set_quad_coord(self):
        if not hasattr(self, 'X'):
            raise ('I do NOT have nodal point coordinates')
        self.X_qp = np.zeros(tuple(np.array([self.dim, self.n_epp, self.n_qp, *(self.N)])))

        if self.element_type == 'linear':
            if self.dim == 2:
                for nodal_point_index in iter_product(self.N):
                    for d in np.arange(self.dim):
                        self.X_qp[(d, 0, 0, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 3
                        self.X_qp[(d, 1, 0, *(nodal_point_index))] = self.X[d][nodal_point_index] + 2 * (self.h[d] / 3)

            if self.dim == 3:
                for nodal_point_index in iter_product(self.N):
                    index = np.array(nodal_point_index)
                    self.X_qp[(0, 0, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + self.h[0] / 2
                    self.X_qp[(0, 1, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + self.h[0] / 2
                    self.X_qp[(0, 2, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + self.h[0] / 3
                    self.X_qp[(0, 3, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + 2 * self.h[0] / 3
                    self.X_qp[(0, 4, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + self.h[0] / 3
                    self.X_qp[(0, 5, 0, *(nodal_point_index))] = self.X[0][nodal_point_index] + 2 * self.h[0] / 3

                    self.X_qp[(1, 0, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + self.h[1] / 3
                    self.X_qp[(1, 1, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + 2 * self.h[1] / 3
                    self.X_qp[(1, 2, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + self.h[1] / 3
                    self.X_qp[(1, 3, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + 2 * self.h[1] / 3
                    self.X_qp[(1, 4, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + self.h[1] / 2
                    self.X_qp[(1, 5, 0, *(nodal_point_index))] = self.X[1][nodal_point_index] + self.h[1] / 2

                    self.X_qp[(2, 0, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + self.h[2] / 3
                    self.X_qp[(2, 1, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + 2 * self.h[2] / 3
                    self.X_qp[(2, 2, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + self.h[2] / 2
                    self.X_qp[(2, 3, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + self.h[2] / 2
                    self.X_qp[(2, 4, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + 2 * self.h[2] / 3
                    self.X_qp[(2, 5, 0, *(nodal_point_index))] = self.X[2][nodal_point_index] + self.h[2] / 3

        elif self.element_type == 'bilinear':
            if self.dim == 2:
                for nodal_point_index in iter_product(self.N):
                    d = 0
                    self.X_qp[(d, 0, 0, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 - (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 1, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 + (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 2, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 - (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 3, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 + (
                            (self.h[d] / 2) / np.sqrt(3))

                    d = 1
                    self.X_qp[(d, 0, 0, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 - (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 1, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 - (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 2, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 + (
                            (self.h[d] / 2) / np.sqrt(3))
                    self.X_qp[(d, 0, 3, *(nodal_point_index))] = self.X[d][nodal_point_index] + self.h[d] / 2 + (
                            (self.h[d] / 2) / np.sqrt(3))
            if self.dim == 3:
                raise ('I do NOT have 3D bilinear stencill')


    def initialize_mat(self, kind='conductivity'):
        if not hasattr(self, 'mat'):
            if kind == 'conductivity':
                self.mat = Tensor(name='A_mat', N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp], shape=2 * [self.dim],
                                  Y=self.Y,
                                  multype='21')
            elif kind == 'elastic':
                self.mat = Tensor(name='C_mat', N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp], shape=4 * [self.dim],
                                  Y=self.Y,
                                  multype='42')

    def get_ref_mat(self, mat_type='mean'):
        if not hasattr(self, 'mat'):
            raise ValueError('Dude, I don NOT have material, yet!')

        if not hasattr(self, 'ref_mat'):
            self.ref_mat = Tensor(name='C_ref_mat', N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp],
                                  shape=self.mat_shape,
                                  Y=self.Y,
                                  multype=self.mat.multype)

        if mat_type == 'mean':
            for alpha in iter_product(self.mat_shape):
                self.ref_mat.val[alpha] = self.mat.val[alpha].mean()

        if mat_type == 'max':
            for alpha in iter_product(self.mat_shape):
                self.ref_mat.val[alpha] = self.mat.val[alpha].max()

        if mat_type == 'min':
            for alpha in iter_product(self.mat_shape):
                self.ref_mat.val[alpha] = self.mat.val[alpha].min()

        if mat_type == 'ones':
            for alpha in iter_product(self.mat_shape):
                if all(x == alpha[0] for x in alpha):
                    self.ref_mat.val[alpha] = 1


    def apply_quadrature_weights(self, var):

        if self.element_type == 'linear':
            w_i = 1
            if self.dim == 1:
                w_i = self.h[0]
            elif self.dim == 2:
                w_i = (self.h[0] * self.h[1]) / 2
            elif self.dim == 3:
                w_i = (self.h[0] * self.h[1] * self.h[2]) / 6

        elif self.element_type == 'bilinear':
            if self.dim == 2:
                w_i = (self.h[0] * self.h[1]) / 4

        var.val = var.val * w_i

    def get_Jacoby_preconditioner(self, prec_type='Jacoby',
                           exp_type=None):

        if self.u_shape == ():
            # stiffnes matrix diagonal
            self.M_Jacoby = Tensor(name='M_Jacoby', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')

            for ijk in iter_product(self.N):
                v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                v_1.val[ijk] = 1

                K_row = self.lhs_periodic(v_1)
                self.M_Jacoby.val[ijk] = copy.deepcopy(1/K_row.val[ijk])


        elif self.u_shape == (self.dim,):
            self.M_Jacoby = Tensor(name='M_Jacoby', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
            for d in np.arange(self.dim):
                for ijk in iter_product(self.N):
                    v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                    v_1.val[(d,) + ijk] = 1
                    K_row = self.lhs_periodic(v_1)
                    self.M_Jacoby.val[d][ijk] = copy.deepcopy(1/K_row.val[d][ijk])


                # K0 = np.reshape(Kijk[0], (np.prod(self.N), np.prod(self.N)))
                # K1 = np.reshape(Kijk[1], (np.prod(self.N), np.prod(self.N)))
                #
                # K[slice(0, np.prod(self.N)), slice(d * np.prod(self.N), (d + 1) * np.prod(self.N))] = K0
                # K[slice(np.prod(self.N), 2 * np.prod(self.N)), slice(d * np.prod(self.N),
                #                                                      (d + 1) * np.prod(self.N))] = K1
                # if self.dim == 3:
                #     K2 = np.reshape(Kijk[2], (np.prod(self.N), np.prod(self.N)))
                #     K[slice(2 * np.prod(self.N), 3 * np.prod(self.N)), slice(d * np.prod(self.N),
                #                                                              (d + 1) * np.prod(self.N))] = K2

        return copy.deepcopy(self.M_Jacoby)


    def get_preconditioner(self, prec_type='diagonal',
                           exp_type=None):  ## TODO check how this preconditioner should work in 3D , 2D FFT
        v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
        index = (0,) * self.dim
        # self.apply_quadrature_weights(var=self.ref_mat) # done for material tensor
        if self.u_shape == ():
            v_1.val[index] = 1
            self.get_grad_periodic(var=v_1)
            v_1.grad.val = self.ref_mat * v_1.grad
            self.get_grad_transpose_periodic(var=v_1)
            # print('imag part norm {}'.format(np.linalg.norm(np.imag(np.fft.fftn(v_1.val)))))
            # print('imag part \n {}'.format(np.imag(np.fft.fftn(v_1.val))))
            v_2 = copy.deepcopy(v_1)
            if exp_type == 'kernel':
                if self.dim == 2:
                    # v_1.val[0,0]=4
                    # v_1.val[1, 0] = -1
                    # v_1.val[0, 1] = -1

                    # v_1.val[-1, 0] = -1
                    # v_1.val[-1, 1] = 0
                    v_1.val[1, 1] = v_1.val[-1, 1]
                    # v_1.val[0, -1] = -1
                    # v_1.val[1, -1] = 0
                    v_1.val[-1, -1] = v_1.val[1, -1]
                    # v_1.val=v_1.val/((self.h[0] * self.h[1]) / 2)

            v_1.val = np.real(np.fft.fftn(v_1.val))
            v_1.val[index] = 1
            v_1.val = v_1.val ** -1
            v_1.val[index] = 0
            self.M_inv = copy.deepcopy(v_1)

        elif self.u_shape == (self.dim,): #  Elastic problem
            if prec_type == 'diagonal':
                self.M_inv = copy.deepcopy(v_1)
                for disp in range(*self.u_shape):
                    v_1.val = np.zeros(v_1.val.shape)

                    v_1.val[(disp,) + index] = 1

                    self.get_grad_periodic(var=v_1)
                    v_1.grad.val = self.ref_mat * v_1.grad
                    self.get_grad_transpose_periodic(var=v_1)

                    self.M_inv.val[disp] = np.real(np.fft.fftn(v_1.val[disp]))
                    self.M_inv.val[(disp,) + index] = 1
                    self.M_inv.val[disp] = self.M_inv.val[disp] ** -1
                    self.M_inv.val[(disp,) + index] = 0
                print()
            elif prec_type == 'full':
                M = {}
                # self.M_inv=np.zeros((self.dim,)+v_1.val.shape)
                self.M_inv = Tensor(name='M_inv', N=self.N, shape=(self.dim,) + self.u_shape, Y=self.Y, multype='21')

                for disp in range(*self.u_shape):
                    v_1.val = np.zeros(v_1.val.shape)
                    # M["M_{0}".format(disp)] =Tensor(name='M_{0}'.format(disp), N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                    v_1.val[(disp,) + index] = 1

                    self.get_grad_periodic(var=v_1)
                    v_1.grad.val = self.ref_mat * v_1.grad
                    self.get_grad_transpose_periodic(var=v_1)

                    for i in range(*self.u_shape):
                        # M["M_{0}".format(disp)].val[i] = np.real(np.fft.fftn(v_1.val[i]))
                        self.M_inv.val[i][disp] = np.real(np.fft.fftn(v_1.val[i]))
                        # M["M_{0}".format(disp)].val[i]
                        # v_1.val[i] = np.real(np.fft.fftn(v_1.val[i]))

                    # v_1.val[i] = np.real(np.fft.fftn(v_1.val[i]))
                    # v_1.val[(i,)+index] = 1
                    # v_1.val[i]  = v_1.val[i]  ** -1
                    # v_1.val[(i,)+index] = 0

                    # self.M_inv.val[disp] = np.real(np.fft.fftn(v_1.val[disp]))
                    # self.M_inv.val[(disp,)+index] = 1
                # self.M_inv.val[disp]  = self.M_inv.val[disp]  ** -1
                # self.M_inv.val[(disp,)+index] = 0
                # print(v_1.val)
                # M.val[(disp,) + index] =copy.deepcopy(v_1.val[(disp,)+index])
                self.M_inv.val[(slice(None, None), slice(None, None), *index)] = np.eye(self.dim, self.dim)
                for ijk in iter_product(self.N):
                    self.M_inv.val[(slice(None, None), slice(None, None), *ijk)] = np.linalg.inv(
                        self.M_inv.val[(slice(None, None), slice(None, None), *ijk)])
                self.M_inv.val[(slice(None, None), slice(None, None), *index)] = np.zeros((self.dim, self.dim))
                print()
            a = 1
            if a == 0:
                raise
        # self.M_inv = copy.deepcopy(v_1)

    def apply_Jacoby_prec(self, var):
        var = copy.deepcopy(var)
        if not hasattr(self, 'M_Jacoby'):
            raise ValueError('Domain has no M_Jacoby.')

        if self.M_Jacoby.shape == ():
            var.val = np.multiply(self.M_Jacoby.val, var.val)

        elif self.M_Jacoby.shape == (self.dim,):
            var.val = np.multiply(self.M_Jacoby.val , var.val)

        return var

    def apply_combined_prec(self, var):
        var = copy.deepcopy(var)
        if not hasattr(self, 'M_Jacoby'):
            raise ValueError('Domain has no M_Jacoby.')
        if not hasattr(self, 'M_inv'):
            raise ValueError('Domain has no M_inv.')

        var.val = np.multiply(self.M_Jacoby.val, var.val)
        var =self.apply_preconditioner(var)

        return var

    def apply_preconditioner(self, var, prec_type='full'):
        var = copy.deepcopy(var)
        if not hasattr(self, 'M_inv'):
            raise ValueError('Domain has no M_inv.')

        # var.val = var.val[:-1:, :-1:]
        # test = copy.deepcopy(var.val)
        if self.u_shape == ():
            # print('imag part norm {}'.format(np.linalg.norm(np.imag(np.fft.fftn(var.val)))))
            # print('imag part {}'.format(np.imag(np.fft.fftn(var.val))))
            var.val = np.fft.fftn(var.val)
        else:
            var.val = var.val.astype('complex128')
            for disp in range(*self.u_shape):
                # print('I am here with disp = {}'.format(disp))
                # print('imag part norm {}'.format(np.linalg.norm(np.imag(np.fft.fftn(var.val[disp])))))
                # print('imag part {}'.format(np.imag(np.fft.fftn(var.val[disp]))))
                var.val[disp] = np.fft.fftn(var.val[disp])
                # print('imag part after norm {}'.format(np.linalg.norm(np.imag(var.val[disp]))))
                # print('imag part after \n {}'.format(np.imag(var.val[disp])))

        if self.M_inv.shape == () or self.M_inv.shape == (self.dim,):
            var.val = np.multiply(self.M_inv.val, var.val)
        elif self.M_inv.shape == (self.dim, self.dim):
            var.val = self.M_inv * var

        if self.u_shape == ():
            var.val = np.fft.ifftn(var.val)
        else:
            for disp in range(*self.u_shape):
                var.val[disp] = np.fft.ifftn(var.val[disp])

        if np.linalg.norm(np.imag(var.val)) > 1e-10:
            print('UNEXPECTED imaginary part norm {} \n {}'.format(var.name, np.linalg.norm(np.imag(var.val))))

        var.val = np.real(var.val)
        return var

    def get_macro_strain(self, var, strain):
        if not hasattr(var, 'grad'):
            var.grad = Tensor(name=' grad {} '.format(var.name), N=self.N, n_epp=[self.n_epp], n_qp=[self.n_qp],
                              Y=self.Y,
                              shape=self.du_shape,
                              multype='vec')

            for ij in iter_product(self.du_shape):
                var.grad.val[ij] = strain[ij]

        return var

    def periodic_DAD(self, var):
        self.get_grad_periodic(var=var)
        var.grad.val = self.mat * var.grad
        var = self.get_grad_transpose_periodic(var=var)

        return var

    def periodic_DD(self, var):
        self.get_grad_periodic(var=var)
        var = self.get_grad_transpose_periodic(var=var)
        return var

    def lhs_periodic(self, var):
        var = copy.deepcopy(var)
        var = self.periodic_DAD(var=var)
        return var

    def rhs_periodic(self, var):
        var = copy.deepcopy(var)
        var.grad.val = self.mat * var.grad
        var = self.get_grad_transpose_periodic(var=var)
        return var

    def prec_lhs_periodic(self, var):
        var = copy.deepcopy(var)
        var = self.periodic_DAD(var=var)
        var = self.apply_preconditioner(var=var)

        return var

    def prec_lhs_periodic_sym(self, var):
        var = copy.deepcopy(var)
        var = self.apply_preconditioner_sym(var=var)
        var = self.periodic_DAD(var=var)
        var = self.apply_preconditioner_sym(var=var)

        return var

    def prec_rhs_periodic(self, var):
        var = copy.deepcopy(var)
        var.grad.val = self.mat * var.grad
        var = self.get_grad_transpose_periodic(var=var)
        var = self.apply_preconditioner(var=var)
        return var

    def compute_e_norm(self, var):
        var.e_norm = np.sum(var.grad.val * (self.mat.val * var.grad.val)) ** 0.5

    def get_x0(self):
        '''
        function create zero initial x0
        -------

        '''
        if not hasattr(self, 'x0'):
            x0 = Tensor(name='x0', N=self.N, shape=self.u_shape, Y=self.Y)
            x0.val = np.zeros(self.u_shape + self.N)
        return x0

    def get_rhs(self, macro_strain):

        rhs = Tensor(name='rhs', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
        rhs = self.get_macro_strain(var=rhs, strain=macro_strain)
        rhs.grad.val = self.mat * rhs.grad
        rhs = self.get_grad_transpose_periodic(var=rhs)
        rhs.val = -rhs.val
        return rhs

    def get_stiffness_matrix(self):
        # return stiffness matrix -- system of linear equation
        if self.u_shape == ():
            Kijk = np.zeros((np.prod(self.N), *self.N))  # stiffnes matrix
            counter = 0
            for ijk in iter_product(self.N):
                v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                v_1.val[ijk] = 1

                K_row = self.lhs_periodic(v_1)
                Kijk[counter] = copy.deepcopy(K_row.val)

                counter = counter + 1

            K = np.reshape(Kijk, (np.prod(self.N), np.prod(self.N)))

        elif self.u_shape == (self.dim,):
            K = np.zeros((np.asarray(self.u_shape)[0] * np.prod(self.N), np.asarray(self.u_shape)[0] * np.prod(self.N)))
            for d in np.arange(self.dim):
                Kijk = np.zeros((self.dim, np.prod(self.N), *self.N))
                counter = 0
                for ijk in iter_product(self.N):
                    v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                    v_1.val[(d,) + ijk] = 1
                    K_row = self.lhs_periodic(v_1)
                    for dir in np.arange(self.dim):
                        Kijk[dir][counter] = copy.deepcopy(K_row.val[dir])
                    counter = counter + 1

                K0 = np.reshape(Kijk[0], (np.prod(self.N), np.prod(self.N)))
                K1 = np.reshape(Kijk[1], (np.prod(self.N), np.prod(self.N)))

                K[slice(0, np.prod(self.N)), slice(d * np.prod(self.N), (d + 1) * np.prod(self.N))] = K0
                K[slice(np.prod(self.N), 2 * np.prod(self.N)), slice(d * np.prod(self.N),
                                                                     (d + 1) * np.prod(self.N))] = K1
                if self.dim == 3:
                    K2 = np.reshape(Kijk[2], (np.prod(self.N), np.prod(self.N)))
                    K[slice(2 * np.prod(self.N), 3 * np.prod(self.N)), slice(d * np.prod(self.N),
                                                                             (d + 1) * np.prod(self.N))] = K2

        return copy.deepcopy(K)

    def get_Prec_stiffness_matrix(self):
        # return preconditioned stiffness matrix -- system of linear equation
        if self.u_shape == ():
            Kijk = np.zeros((np.prod(self.N), *self.N))  # stiffnes matrix
            counter = 0
            for ijk in iter_product(self.N):
                v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                v_1.val[ijk] = 1

                K_row = self.prec_lhs_periodic(v_1)
                Kijk[counter] = copy.deepcopy(K_row.val)

                counter = counter + 1

            K = np.reshape(Kijk, (np.prod(self.N), np.prod(self.N)))

        elif self.u_shape == (self.dim,):
            K = np.zeros((np.asarray(self.u_shape)[0] * np.prod(self.N), np.asarray(self.u_shape)[0] * np.prod(self.N)))
            for d in np.arange(self.dim):
                Kijk = np.zeros((self.dim, np.prod(self.N), *self.N))
                counter = 0
                for ijk in iter_product(self.N):
                    v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                    v_1.val[(d,) + ijk] = 1
                    K_row = self.prec_lhs_periodic(v_1)
                    for dir in np.arange(self.dim):
                        Kijk[dir][counter] = copy.deepcopy(K_row.val[dir])
                    counter = counter + 1

                K0 = np.reshape(Kijk[0], (np.prod(self.N), np.prod(self.N)))
                K1 = np.reshape(Kijk[1], (np.prod(self.N), np.prod(self.N)))

                K[slice(0, np.prod(self.N)), slice(d * np.prod(self.N), (d + 1) * np.prod(self.N))] = K0
                K[slice(np.prod(self.N), 2 * np.prod(self.N)), slice(d * np.prod(self.N),
                                                                     (d + 1) * np.prod(self.N))] = K1
                if self.dim == 3:
                    K2 = np.reshape(Kijk[2], (np.prod(self.N), np.prod(self.N)))
                    K[slice(2 * np.prod(self.N), 3 * np.prod(self.N)), slice(d * np.prod(self.N),
                                                                             (d + 1) * np.prod(self.N))] = K2

        return copy.deepcopy(K)

    def get_Prec_matrix(self):
        # return preconditioned stiffness matrix -- system of linear equation
        if self.u_shape == ():
            Mijk = np.zeros((np.prod(self.N), *self.N))  # stiffnes matrix
            counter = 0
            for ijk in iter_product(self.N):
                v_1 = Tensor(name='v_1', N=self.N, shape=self.u_shape, Y=self.Y, multype='scal')
                v_1.val[ijk] = 1

                M_row = self.apply_preconditioner(v_1)
                Mijk[counter] = copy.deepcopy(M_row.val)

                counter = counter + 1

            K = np.reshape(Mijk, (np.prod(self.N), np.prod(self.N)))

        return copy.deepcopy(K)
