# µFFT-FEM

Micromechanical Finite Element solver. Discretisation on a regular Cartesian grid with material-based preconditioner applied via the Fast Fourier Transform. Implemented in Python 3.7.
